<?php
/**
 * @version    1.0
 * @package    VG Galio
 * @author     VinaGecko Team <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

add_action('wp_enqueue_scripts', 'galio_child_enqueue_styles', 10000);
function galio_child_enqueue_styles() {
    wp_enqueue_style('vg-galio-child-style', get_stylesheet_directory_uri() . '/style.css' );
}