<?php
$galio_options  = galio_get_global_variables();

$webLayout = galio_get_layout();

switch($webLayout)
{
	case "layout-1":
		require(get_template_directory() . "/vgwc-themes/galio/layout-1.php");
	break;
	case "layout-2":
		require(get_template_directory() . "/vgwc-themes/galio/layout-2.php");
	break;
	case "layout-3":
		require(get_template_directory() . "/vgwc-themes/galio/layout-3.php");
	break;
	case "layout-4":
		require(get_template_directory() . "/vgwc-themes/galio/layout-4.php");
	break;
	default:
		require(get_template_directory() . "/vgwc-themes/galio/layout-1.php");
	break;
}