<?php
/**
 * @package  VG MegaMain Widget
 * @subpackage MegaMain
 * 
 */
 
class Vg_Postcarousel extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__('Add a "VG Postcarousel" to your widget.','galio') );
		parent::__construct( 'vg_Postcarousel', esc_html__('VG Postcarousel Widget','galio'), $widget_ops );
	}

	function widget($args, $instance) {

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];
		
		$output = '<div class="wpb_text_column">';
		
		if ( $title ) {
			$output .= $args['before_title'] . $title .$args['after_title'];
		}
		$output .=  do_shortcode("[vgpc id='" . $instance['postcarouselid'] . "']") ;

		$output .=  '</div>';
		
        echo $output;

        echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] 				= strip_tags( stripslashes($new_instance['title']) );
		$instance['postcarouselid'] 	= (int) $new_instance['postcarouselid'];
		
		return $instance;
	}

	function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? $instance['title'] : '';
		$postcarouselid 	= isset( $instance['postcarouselid'] ) ? $instance['postcarouselid'] : '';
		
		$args = array(
			'post_type' => 'vgpc',
			'posts_per_page' => -1,
		);

		$vgpc = new WP_Query($args);
		if ($vgpc->have_posts()) : 
			while ($vgpc->have_posts()) : $vgpc->the_post(); 
	
				$vgpctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;

		// If no menus exists, direct the user to go and create some.
		if ( !$vgpctitle ) {
			echo '<p>'. esc_html__('No Postcarousel have been created yet.','galio');
			return;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo esc_html__('Title:','galio') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('postcarouselid'); ?>"><?php echo esc_html__('Select Postcarousel:','galio'); ?></label>
			<select id="<?php echo $this->get_field_id('postcarouselid'); ?>" name="<?php echo $this->get_field_name('postcarouselid'); ?>">
		<?php
			foreach ( $vgpctitle as $vgpcid ) {
				$postcarousel = get_post($vgpcid);
				echo '<option value="' . $vgpcid . '"'
					. selected( $postcarouselid, $vgpcid, false )
					. '>'. $postcarousel->post_title . '</option>';
			}
		?>
			</select>
		</p>
		<?php
	}
}

register_widget( 'Vg_Postcarousel' );