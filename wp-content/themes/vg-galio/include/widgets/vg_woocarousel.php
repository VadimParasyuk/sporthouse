<?php
/**
 * @package  VG MegaMain Widget
 * @subpackage MegaMain
 * 
 */
 
class Vg_Woocarousel extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__('Add a "VG WooCarousel" to your widget.','galio') );
		parent::__construct( 'vg_woocarousel', esc_html__('VG WooCarousel Widget','galio'), $widget_ops );
	}

	function widget($args, $instance) {

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];
		$output = '<div class="wpb_text_column">';
		if ( $title ) {
			$output .= $args['before_title'] . $title .$args['after_title'];
		}
		$output .=  do_shortcode("[vgwc id='" . $instance['woocarouselid'] . "']") ;
		
		$output .=  '</div>';

        echo $output;

        echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] 				= strip_tags( stripslashes($new_instance['title']) );
		$instance['woocarouselid'] 			= (int) $new_instance['woocarouselid'];
		
		return $instance;
	}

	function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? $instance['title'] : '';
		$woocarouselid 	= isset( $instance['woocarouselid'] ) ? $instance['woocarouselid'] : '';
		
		$args = array(
			'post_type' => 'vgwc',
			'posts_per_page' => -1,
		);

		$vgwc = new WP_Query($args);
		if ($vgwc->have_posts()) : 
			while ($vgwc->have_posts()) : $vgwc->the_post(); 
	
				$vgwctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;

		// If no menus exists, direct the user to go and create some.
		if ( !$vgwctitle ) {
			echo '<p>'. esc_html__('No WooCarousel have been created yet.','galio');
			return;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo esc_html__('Title:','galio') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('woocarouselid'); ?>"><?php echo esc_html__('Select WooCarousel:','galio'); ?></label>
			<select id="<?php echo $this->get_field_id('woocarouselid'); ?>" name="<?php echo $this->get_field_name('woocarouselid'); ?>">
		<?php
			foreach ( $vgwctitle as $vgwcid ) {
				$woocarousel = get_post($vgwcid);
				echo '<option value="' . $vgwcid . '"'
					. selected( $woocarouselid, $vgwcid, false )
					. '>'. $woocarousel->post_title . '</option>';
			}
		?>
			</select>
		</p>
		<?php
	}
}

register_widget( 'Vg_Woocarousel' );