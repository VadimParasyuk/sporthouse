<?php
/**
 * @package  VG MegaMain Widget
 * @subpackage MegaMain
 * 
 */
 
class Vg_Mega_menu extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__('Add a "VG Mega Main Menu" to your widget.','galio') );
		parent::__construct( 'vg_mega_main_sidebar_menu', esc_html__('VG Mega Menu Widget','galio'), $widget_ops );
		$this->widget_options['classname'].= ' vg_mega_menu widget_mega_main_sidebar_menu';
	}

	function widget($args, $instance) {
		// Get menu
		$nav_menu 		 = ! empty( $instance['nav_menu'] ) ? $instance['nav_menu'] : false;
		$nav_menu_mobile = ! empty( $instance['nav_menu_mobile'] ) ? $instance['nav_menu_mobile'] : false;
		
		if ( !$nav_menu && !$nav_menu_mobile) {
			return;
		}	
		
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		echo $args['before_widget'];
		if ( !empty($instance['title']) ) { 
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		wp_nav_menu( array( 'theme_location' => $nav_menu, 'menu' => $nav_menu ) );
		
		$defaults = array(
			'theme_location' => $nav_menu_mobile,
			'container_class' => 'mobile-mega-menu hidden-lg hidden-md',
			'menu_class' => 'nav-menu',
		);

		wp_nav_menu( $defaults );
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] 				= strip_tags( stripslashes($new_instance['title']) );
		$instance['nav_menu'] 			= strip_tags($new_instance['nav_menu']); 
		$instance['nav_menu_mobile']	= strip_tags($new_instance['nav_menu_mobile']);
		
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$nav_menu = isset( $instance['nav_menu'] ) ? $instance['nav_menu'] : '';
		$nav_menu_mobile = isset( $instance['nav_menu_mobile'] ) ? $instance['nav_menu_mobile'] : '';

		// Get menus
		$menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
		
		foreach ( get_registered_nav_menus() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
		foreach ( get_nav_menu_locations() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
		

		// If no menus exists, direct the user to go and create some.
		if ( !$menus ) {
			echo '<p>'. wp_kses( __('No menus have been created yet. <a href="%s">Create some</a>.','galio'), admin_url('nav-menus.php'), array('a' => array('href' => array()))) .'</p>';
			return;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo esc_html__('Title:','galio') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php echo esc_html__('Select Mega Menu Location:','galio'); ?></label>
			<select id="<?php echo $this->get_field_id('nav_menu'); ?>" name="<?php echo $this->get_field_name('nav_menu'); ?>">
		<?php	
			foreach ( $theme_menu_locations as $key ) {
				echo '<option value="' . $theme_menu_locations[ $key ] . '"'
					. selected( $nav_menu,$theme_menu_locations[ $key ], false )
					. '>'. $theme_menu_locations[ $key ] . '</option>';
			}
		?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('nav_menu_mobile'); ?>"><?php echo esc_html__('Select Mobile Menu Location:','galio'); ?></label>
			<select id="<?php echo $this->get_field_id('nav_menu_mobile'); ?>" name="<?php echo $this->get_field_name('nav_menu_mobile'); ?>">
		<?php
			foreach ( $theme_menu_locations as $key ) {
				echo '<option value="' . $theme_menu_locations[ $key ] . '"'
					. selected( $nav_menu_mobile,$theme_menu_locations[ $key ], false )
					. '>'. $theme_menu_locations[ $key ] . '</option>';
			}
		?>
			</select>
		</p>
		<?php
	}
}

register_widget( 'Vg_Mega_menu' );
?>