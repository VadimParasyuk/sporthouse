<?php
//Shortcodes for Visual Composer

add_action('vc_before_init', 'galio_vc_shortcodes');
function galio_vc_shortcodes() {
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		$args = array(
			'post_type' => 'vgwc',
			'posts_per_page' => -1,
		);

		$vgwc = new WP_Query($args);
		
		if ($vgwc->have_posts()) : 
			while ($vgwc->have_posts()) : $vgwc->the_post(); 
				$vgwctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
	}	
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		$args = array(
			'post_type' => 'vgpc',
			'posts_per_page' => -1,
		);

		$vgpc = new WP_Query($args);
		
		if ($vgpc->have_posts()) : 
			while ($vgpc->have_posts()) : $vgpc->the_post(); 
				$vgpctitle[get_the_title()]= get_the_ID();
			endwhile;
		endif;
	}
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		foreach ( get_registered_nav_menus() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
		foreach ( get_nav_menu_locations() as $key => $value ){
			$key = str_replace( ' ', '-', $key );
			$theme_menu_locations[ $key ] = $key;
		}
	}
	//End Swallow Code Custom elements Visual Composer
	
	if ( is_plugin_active( 'mega_main_menu/mega_main_menu.php' ) ) {
		//VG MegaMenu && Custom Menu
		vc_map(array(
			'name' => esc_html__('VG MegaMenu', 'galio'),
			'base' => 'vgmegamenu',
			'icon' => 'icon-wpb-vg icon-wpb-megamenu',
			'class' => '',
			'category' => esc_html__('VG Galio', 'galio'),
			'description' => esc_html__('VG MegaMain Extensions Replace', 'galio'),
			'admin_label' => true,
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'galio' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'What text use as a widget title. Leave blank to use default widget title.', 'galio' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'galio' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'galio' ),
				),
				array(
					'heading' =>  esc_html__( 'Select Mega Menu Desktop', 'galio' ),
					'description' =>  esc_html__( 'Select Mega menu in desktop.', 'galio' ),
					'param_name' => 'mega_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'heading' =>  esc_html__( 'Select Treeview Menu Mobile', 'galio' ),
					'description' => empty( $custom_menus ) ?  esc_html__( 'Custom menus not found. Please visit <b>Appearance > Menus</b> page to create new menu.', 'galio' ) :  esc_html__( 'Select menu to display.', 'galio' ),
					'param_name' => 'nav_menu',
					'type' => 'dropdown',
					'value' => $theme_menu_locations,
					'admin_label' => true,
					'save_always' => true,
				),	
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'galio' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'galio' ),
				),
			)
		));
	}
	//Brand logos
	vc_map(array(
		'name' => esc_html__('Brand Logos', 'galio'),
		'base' => 'ourbrands',
		'icon' => 'icon-wpb-vg icon-wpb-ourbrands',
		'class' => '',
		'category' => esc_html__('VG Galio', 'galio'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Widget title', 'galio' ),
				'param_name' => 'title',
				'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'galio' ),
			),
			array(
				'type' => 'css_editor',
				'heading' =>  esc_html__( 'CSS box', 'galio' ),
				'param_name' => 'css',
				'group' =>  esc_html__( 'Design Options', 'galio' ),
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'class' => '',
				'heading' => esc_html__('Number of rows', 'galio'),
				'param_name' => 'rowsnumber',
				'value' => array(
					'one'	=> 'one',
					'two'	=> 'two',
					'four'	=> 'four',
				),
			),
			array(
				'type' => 'checkbox',
				'heading' =>  esc_html__( 'Show Icon?', 'galio' ),
				'param_name' => 'add_show_icon',
				'description' =>  esc_html__( 'Show Icon.', 'galio' ),
				'value' => array(  esc_html__( 'Yes', 'galio' ) => 'Yes' ),
				'save_always' => true,
			),
			array(
				'type' => 'dropdown',
				'heading' =>  esc_html__( 'Icon library', 'galio' ),
				'value' => array(
					 esc_html__( 'Font Awesome', 'galio' ) => 'fontawesome',
					 esc_html__( 'Open Iconic', 'galio' ) => 'openiconic',
					 esc_html__( 'Typicons', 'galio' ) => 'typicons',
					 esc_html__( 'Entypo', 'galio' ) => 'entypo',
					 esc_html__( 'Linecons', 'galio' ) => 'linecons',
				),
				'save_always' => true,
				'admin_label' => true,
				'param_name' => 'type',
				'description' =>  esc_html__( 'Select icon library.', 'galio' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'galio' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an 'EMPTY' icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'galio' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'galio' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'galio' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' =>  esc_html__( 'Icon', 'galio' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'save_always' => true,
				'settings' => array(
					'emptyIcon' => false, // default true, display an 'EMPTY' icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
			),
			array(
				'type' => 'textfield',
				'heading' =>  esc_html__( 'Extra class name', 'galio' ),
				'param_name' => 'el_class',
				'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'galio' ),
			),
		)
	));
	
	//Testimonials
	vc_map( array(
		"name" => esc_html__( "Testimonials", "galio" ),
		"base" => "woothemes_testimonials",
		"class" => "",
		"category" => esc_html__( "VG Galio", "galio"),
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Number of testimonial", "galio" ),
				"param_name" => "limit",
				"value" => esc_html__( "10", "galio" ),
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__( "Image size", "galio" ),
				"param_name" => "size",
				"value" => esc_html__( "120", "galio" ),
			),
		)
	) );
	
	//Swallow Code Custom elements Visual Composer
	if ( is_plugin_active( 'vg-woocarousel/vg-woocarousel.php' ) ) {
		//List VG WooCarousel
		vc_map(array(
			'name' => esc_html__('VG WooCarousel', 'galio'),
			'base' => 'listvgwccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-woocarousel',
			'class' => '',
			'category' => esc_html__('VG Galio', 'galio'),
			'description' => esc_html__('VG WooCarousel Extensions', 'galio'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'galio' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'galio' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'galio' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'galio' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'galio'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => $vgwctitle,
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'galio' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'galio' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'galio' ),
					'value' => array(  esc_html__( 'Yes', 'galio' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'galio' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'galio' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'galio' ) => 'openiconic',
						 esc_html__( 'Typicons', 'galio' ) => 'typicons',
						 esc_html__( 'Entypo', 'galio' ) => 'entypo',
						 esc_html__( 'Linecons', 'galio' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'galio' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'galio' ),
				),
			)
		));
	}
	
	if ( is_plugin_active( 'vg-postcarousel/vg-postcarousel.php' ) ) {
		//List VG PostCarousel
		vc_map(array(
			'name' => esc_html__('VG PostCarousel', 'galio'),
			'base' => 'listvgpccarousel',
			'icon' => 'icon-wpb-vg icon-wpb-postcarousel',
			'class' => '',
			'category' => esc_html__('VG Galio', 'galio'),
			'description' => esc_html__('VG PostCarousel Extensions', 'galio'),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Widget title', 'galio' ),
					'param_name' => 'title',
					'description' =>  esc_html__( 'Enter text used as widget title (Note: located above content element).', 'galio' ),
				),
				array(
					'type' => 'css_editor',
					'heading' =>  esc_html__( 'CSS box', 'galio' ),
					'param_name' => 'css',
					'group' =>  esc_html__( 'Design Options', 'galio' ),
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('VG WooCarousel', 'galio'),
					'param_name' => 'alias',
					'admin_label' => true,
					'value' => $vgpctitle,
					'save_always' => true,
					'description' =>  esc_html__( 'Select your VG WooCarousel.', 'galio' ),
				),
				array(
					'type' => 'checkbox',
					'heading' =>  esc_html__( 'Show Icon?', 'galio' ),
					'param_name' => 'add_show_icon',
					'description' =>  esc_html__( 'Show Icon.', 'galio' ),
					'value' => array(  esc_html__( 'Yes', 'galio' ) => 'Yes' ),
					'save_always' => true,
				),
				array(
					'type' => 'dropdown',
					'heading' =>  esc_html__( 'Icon library', 'galio' ),
					'value' => array(
						 esc_html__( 'Font Awesome', 'galio' ) => 'fontawesome',
						 esc_html__( 'Open Iconic', 'galio' ) => 'openiconic',
						 esc_html__( 'Typicons', 'galio' ) => 'typicons',
						 esc_html__( 'Entypo', 'galio' ) => 'entypo',
						 esc_html__( 'Linecons', 'galio' ) => 'linecons',
					),
					'save_always' => true,
					'admin_label' => true,
					'param_name' => 'type',
					'description' =>  esc_html__( 'Select icon library.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an 'EMPTY' icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' =>  esc_html__( 'Icon', 'galio' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'save_always' => true,
					'settings' => array(
						'emptyIcon' => false, // default true, display an 'EMPTY' icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' =>  esc_html__( 'The Always select icon from the library if you want to have the icon.', 'galio' ),
				),
				array(
					'type' => 'textfield',
					'heading' =>  esc_html__( 'Extra class name', 'galio' ),
					'param_name' => 'el_class',
					'description' =>  esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'galio' ),
				),
			)
		));
	}
	//End Swallow Code Custom elements Visual Composer
}
?>