<?php
/**
 * @version    1.6
 * @package    VG Galio
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

//Require plugins
require_once get_template_directory() . '/class-tgm-plugin-activation.php';

function galio_register_required_plugins()
{

    $plugins = array(
        array(
            'name' => esc_html__('VinaGecko Helper', 'galio'),
            'slug' => 'vinagecko-helper',
            'source' => get_template_directory() . '/plugins/vinagecko-helper.zip',
            'required' => true,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Mega Main Menu', 'galio'),
            'slug' => 'mega_main_menu',
            'source' => get_template_directory() . '/plugins/mega_main_menu.zip',
            'required' => true,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Visual Composer', 'galio'),
            'slug' => 'js_composer',
            'source' => get_template_directory() . '/plugins/js_composer.zip',
            'required' => true,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Redux Framework', 'galio'),
            'slug' => 'redux-framework',
            'source' => get_template_directory() . '/plugins/redux-framework.zip',
            'required' => true,
            'force_activation' => false,
            'force_deactivation' => false,
        ),
        array(
            'name' => esc_html__('VG WooCarousel', 'galio'),
            'slug' => 'vg-woocarousel',
            'source' => esc_url('http://wordpress.vinagecko.net/l/vg-woocarousel.zip'),
            'required' => true,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('VG PostCarousel', 'galio'),
            'slug' => 'vg-postcarousel',
            'source' => esc_url('http://wordpress.vinagecko.net/l/vg-postcarousel.zip'),
            'required' => true,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Revolution Slider', 'galio'),
            'slug' => 'revslider',
            'source' => esc_url('http://wordpress.vinagecko.net/l/revslider.zip'),
            'required' => true,
            'external_url' => '',
        ),
        // Plugins from the WordPress Plugin Repository.
        array(
            'name' => esc_html__('Shortcodes Ultimate', 'galio'),
            'slug' => 'shortcodes-ultimate',
            'required' => true,
            'force_activation' => false,
            'force_deactivation' => false,
        ),
        array(
            'name' => esc_html__('Contact Form 7', 'galio'),
            'slug' => 'contact-form-7',
            'required' => true,
        ),
        array(
            'name' => esc_html__('MailPoet Newsletters', 'galio'),
            'slug' => 'wysija-newsletters',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Projects', 'galio'),
            'slug' => 'projects-by-woothemes',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Testimonials', 'galio'),
            'slug' => 'testimonials-by-woothemes',
            'required' => true,
        ),
        array(
            'name' => esc_html__('TinyMCE Advanced', 'galio'),
            'slug' => 'tinymce-advanced',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Image Widget', 'galio'),
            'slug' => 'image-widget',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Widget Importer & Exporter', 'galio'),
            'slug' => 'widget-importer-exporter',
            'required' => true,
        ),
        array(
            'name' => esc_html__('WordPress Importer', 'galio'),
            'slug' => 'wordpress-importer',
            'required' => true,
        ),
        array(
            'name' => esc_html__('WooCommerce', 'galio'),
            'slug' => 'woocommerce',
            'required' => true,
        ),
        array(
            'name' => esc_html__('WordPress AJAX Search & AutoSuggest Plugin', 'galio'),
            'slug' => 'ajax-search-autosuggest',
            'required' => true,
            'source' => esc_url('http://wordpress.vinagecko.net/l/ajax-autosuggest-plugin.zip'),
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Compare', 'galio'),
            'slug' => 'yith-woocommerce-compare',
            'required' => true,
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Multi-step Checkout', 'galio'),
            'slug' => 'yith-woocommerce-multi-step-checkout',
            'required' => true,
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Wishlist', 'galio'),
            'slug' => 'yith-woocommerce-wishlist',
            'required' => true,
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Zoom Magnifier', 'galio'),
            'slug' => 'yith-woocommerce-zoom-magnifier',
            'required' => true,
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'has_notices' => true,                    // Show admin notices or not.
        'dismissable' => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message' => '',                      // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => esc_html__('Install Required Plugins', 'galio'),
            'menu_title' => esc_html__('Install Plugins', 'galio'),
            'installing' => esc_html__('Installing Plugin: %s', 'galio'), // %s = plugin name.
            'oops' => esc_html__('Something went wrong with the plugin API.', 'galio'),
            'notice_can_install_required' => _n_noop('This g requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'galio'), // %1$s = plugin name(s).
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'galio'), // %1$s = plugin name(s).
            'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'galio'), // %1$s = plugin name(s).
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'galio'), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'galio'), // %1$s = plugin name(s).
            'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'galio'), // %1$s = plugin name(s).
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'galio'), // %1$s = plugin name(s).
            'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'galio'), // %1$s = plugin name(s).
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'galio'),
            'activate_link' => _n_noop('Begin activating plugin', 'Begin activating plugins', 'galio'),
            'return' => esc_html__('Return to Required Plugins Installer', 'galio'),
            'plugin_activated' => esc_html__('Plugin activated successfully.', 'galio'),
            'complete' => esc_html__('All plugins installed and activated successfully. %s', 'galio'), // %s = dashboard link.
            'nag_type' => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa($plugins, $config);

}

add_action('tgmpa_register', 'galio_register_required_plugins');

//Init the Redux Framework
if (class_exists('ReduxFramework') && !isset($redux_demo) && file_exists(get_template_directory() . '/theme-config.php')) {
    require_once(get_template_directory() . '/theme-config.php');
}

//Add Woocommerce support
add_theme_support('woocommerce');
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

function galio_start_wrapper_here()
{ // Start wrapper

    if (is_shop()) { // Wrapper for the shop page

        echo '<div class="shop-wrapper">';

    } elseif (is_product_category() || is_product_tag()) {  // Wrapper for a product category page

        echo '<div class="product-category-wrapper">';

    } elseif (is_product()) { // Wrapper for a single product page

        echo '<div class="product-wrapper">';

    }
}

add_action('woocommerce_before_main_content', 'galio_start_wrapper_here', 20);


add_action('woocommerce_before_main_content', 'galio_start_wrapper_here_end', 50);
function galio_start_wrapper_here_end()
{ // Start wrapper
    echo '</div>';
}

//Override woocommerce widgets
function galio_override_woocommerce_widgets()
{
    //Show mini cart on all pages
    if (class_exists('WC_Widget_Cart')) {
        unregister_widget('WC_Widget_Cart');
        include_once(get_template_directory() . '/woocommerce/class-wc-widget-cart.php');
        register_widget('Custom_WC_Widget_Cart');
    }
}

add_action('widgets_init', 'galio_override_woocommerce_widgets', 15);

// Swallow code Override ajax search autosuggest widget
//Override woocommerce widgets
function galio_override_ajaxsearch_autosuggest_widgets()
{
    //Show mini cart on all pages
    if (class_exists('Ajax_search_widget')) {
        unregister_widget('Ajax_search_widget');
        include_once(get_template_directory() . '/include/ajax-search-autosuggest.php');
        register_widget('Custom_Ajax_search_widget');
    }
}

add_action('widgets_init', 'galio_override_ajaxsearch_autosuggest_widgets', 15);

add_filter('woocommerce_product_get_rating_html', 'galio_get_rating_html', 10, 2);

function galio_get_rating_html($rating_html, $rating)
{
    global $product;
    if ($rating > 0) {
        $title = sprintf(esc_html__('Rated %s out of 5', 'galio'), $rating);
    } else {
        $title = 'Not yet rated';
        $rating = 0;
    }

    $rating_html = '<div class="vgwc-product-rating">';
    $rating_html .= '<div class="star-rating" title="' . $title . '">';
    $rating_html .= '<span style="width:' . (($rating / 5) * 100) . '%"><strong class="rating">' . $rating . '</strong> ' . esc_html__(' 5', 'galio') . '</span>';
    $rating_html .= '</div>';
    $rating_html .= $product->get_review_count() . esc_html__(" Рейтинг", "galio");
    $rating_html .= '</div>';

    return $rating_html;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
function galio_woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    ?>

    <span class="mcart-number"><?php echo WC()->cart->cart_contents_count; ?></span>

    <?php
    $fragments['span.mcart-number'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'galio_woocommerce_header_add_to_cart_fragment');

//Change price html
add_filter('woocommerce_get_price_html', 'galio_woo_price_html', 100, 2);
function galio_woo_price_html($price, $product)
{
    if ($product->is_type('variable')) {
        return '<div class="vgwc-product-price price-variable">' . $price . '</div>';
    } else {
        return '<div class="vgwc-product-price">' . $price . '</div>';
    }
}

remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
// Add image to category description
function galio_woocommerce_category_image()
{
    global $wp_query, $galio_options;
    if (is_product_category() || is_shop() || is_product_tag()) {
        $image = '';
        if (is_product_category() || is_product_tag()) {
            $cat = $wp_query->get_queried_object();
            $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
            $image = wp_get_attachment_url($thumbnail_id);
        }
        if (empty($cat->description) && isset($galio_options['cat_description']) && !empty($galio_options['cat_description'])) {
            ?>
            <div class="term-description vg-description"><?php echo $galio_options['cat_description']; ?></div><?php
        } else if (!empty($cat->description)) {
            ?>
            <div class="term-description"><?php echo $cat->description ?></div><?php
        }
        if (($image && !empty($image))) { ?>
            <div class="category-image-desc"><img src="<?php echo esc_url($image); ?>" alt=""></div>
        <?php } else if (isset($galio_options['cat_banner_link']) && !empty($galio_options['cat_banner_link']) && isset($galio_options['cat_banner_img']) && !empty($galio_options['cat_banner_img'])) { ?>
            <div class="category-image-desc vg-cat-img"><a
                    href="<?php echo esc_url($galio_options['cat_banner_link']); ?>"><img
                        src="<?php echo esc_url($galio_options['cat_banner_img']['url']); ?>" alt=""></a></div>
        <?php } else if (isset($galio_options['cat_banner_img']) && !empty($galio_options['cat_banner_img'])) { ?>
            <div class="category-image-desc vg-cat-img"><img
                    src="<?php echo esc_url($galio_options['cat_banner_img']['url']); ?>" alt=""></div>
        <?php }
    }
}

add_action('woocommerce_archive_description', 'galio_woocommerce_category_image', 20);

// Change products per page
function galio_woo_change_per_page()
{
    global $galio_options;

    return $galio_options['product_per_page'];
}

add_filter('loop_shop_per_page', 'galio_woo_change_per_page', 20);

function galio_copyright()
{
    global $galio_options;
    $copynotice = (isset($galio_options['copyright-notice'])) ? $galio_options['copyright-notice'] : '';
    $copylink = (isset($galio_options['copyright-link'])) ? $galio_options['copyright-link'] : '';
    if (strpos($copynotice, '{') && strpos($copynotice, '}') && $copylink) {
        $copyright = str_ireplace('{', '<a href="' . $copylink . '">', $copynotice);
        $copyright = str_ireplace('}', '</a>', $copyright);
    } else {
        $copyright = $copynotice;
    }
    return $copyright;
}

//Limit number of products by shortcode [products]
//add_filter( 'woocommerce_shortcode_products_query', 'galio_woocommerce_shortcode_limit' );
function galio_woocommerce_shortcode_limit($args)
{
    global $galio_options, $galio_productsfound;

    if (isset($galio_options['shortcode_limit']) && $args['posts_per_page'] == -1) {
        $args['posts_per_page'] = $galio_options['shortcode_limit'];
    }

    $galio_productsfound = new WP_Query($args);
    $galio_productsfound = $galio_productsfound->post_count;

    return $args;
}

// Change number or products per row to 4
function galio_loop_columns()
{
    global $galio_options;

    return $galio_options['product_per_row'];
}

add_filter('loop_shop_columns', 'galio_loop_columns', 999);

//Change number of related products on product page. Set your own value for 'posts_per_page'
function galio_woo_related_products_limit($args)
{
    global $product, $galio_options;
    $args['posts_per_page'] = $galio_options['related_amount'];

    return $args;
}

add_filter('woocommerce_output_related_products_args', 'galio_woo_related_products_limit');

//move message to top
remove_action('woocommerce_before_shop_loop', 'wc_print_notices', 10);
add_action('woocommerce_show_message', 'wc_print_notices', 10);


function galio_view_mode_woocommerce_shop_loop()
{
    global $galio_options;
    ?>
    <script>
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                jQuery('.view-mode').each(function () {
                    <?php if($galio_options['layout_product'] == 'gridview') { ?>
                    /* Grid View */
                    jQuery('#archive-product .view-mode').find('.grid').addClass('active');
                    jQuery('#archive-product .view-mode').find('.list').removeClass('active');

                    jQuery('#archive-product .shop-products').removeClass('list-view');
                    jQuery('#archive-product .shop-products').addClass('grid-view');

                    jQuery('#archive-product .list-col4').removeClass('col-xs-12 col-sm-4');
                    jQuery('#archive-product .list-col8').removeClass('col-xs-12 col-sm-8');
                    <?php } ?>
                    <?php if($galio_options['layout_product'] == 'listview') { ?>
                    /* List View */
                    jQuery('#archive-product .view-mode').find('.list').addClass('active');
                    jQuery('#archive-product .view-mode').find('.grid').removeClass('active');

                    jQuery('#archive-product .shop-products').addClass('list-view');
                    jQuery('#archive-product .shop-products').removeClass('grid-view');

                    jQuery('#archive-product .list-col4').addClass('col-xs-12 col-sm-4');
                    jQuery('#archive-product .list-col8').addClass('col-xs-12 col-sm-8');
                    <?php } ?>
                });
            });
        })(jQuery);
    </script>
    <?php
}

add_filter('woocommerce_before_shop_loop', 'galio_view_mode_woocommerce_shop_loop', 5);
add_filter('woocommerce_after_shop_loop', 'galio_view_mode_woocommerce_shop_loop', 5);

add_action('woocommerce_after_shop_loop', 'woocommerce_result_count', 10);

/*Remove product link open*/
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals');

/*Close div*/

//Single product organize
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('woocommerce_show_related_products', 'woocommerce_output_related_products', 20);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_short_description', 10);

// Add previous and next links to products under the product details
add_action('woocommerce_single_product_summary', 'galio_next_prev_products_links', 1);
function galio_next_prev_products_links()
{
    ?>
    <div class="product-nav pull-right">
        <div class="next-prev">
            <div class="prev"><?php previous_post_link('%link'); ?></div>
            <div class="next"><?php next_post_link('%link'); ?></div>
        </div>
    </div>
    <?php
}

function galio_single_meta()
{
    global $galio_options;
    //remove single meta
    if (isset($galio_options['single_meta']) && $galio_options['single_meta']) {
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    }
}

add_action('init', 'galio_single_meta');

//Display social sharing on product page
function galio_woocommerce_social_share()
{
    global $galio_options;
    ?>
    <div class="share_buttons">
        <?php if ($galio_options['share_code'] != '') {
            echo wp_kses($galio_options['share_code'], array(
                'div' => array(
                    'class' => array()
                ),
                'span' => array(
                    'class' => array(),
                    'displayText' => array()
                ),
            ));
        } ?>
    </div>
    <?php
}

add_action('vg_social_share', 'galio_woocommerce_social_share', 35);

//Display stock status on product page
function galio_product_stock_status()
{
    global $product;
    ?>
    <div class="in-stock">
        <?php esc_html_e('Доступно:', 'galio'); ?>
        <?php if ($product->is_in_stock()) { ?>
            <span><?php echo $product->get_stock_quantity() . " "; ?><?php esc_html_e('На складе', 'galio'); ?></span>
        <?php } else { ?>
            <span class="out-stock"><?php esc_html_e('Нет', 'galio'); ?></span>
        <?php } ?>
    </div>
    <?php
}

add_action('woocommerce_single_product_summary', 'galio_product_stock_status', 15);

//Show countdown on product page
function galio_product_countdown()
{
    global $product;
    $product_countdown = '';
    if ($product->is_in_stock()) {
        ?>
        <?php
        $countdown = false;
        $sale_end = get_post_meta($product->id, '_sale_price_dates_to', true);
        /* simple product */
        if ($sale_end) {
            $countdown = true;
            $sale_end = date('Y/m/d', (int)$sale_end);
            ?>
            <?php $product_countdown .= '<div class="box-timer"><div class="timer-grid" data-time="' . esc_attr($sale_end) . '"></div></div>'; ?>
        <?php } ?>
        <?php /* variable product */
        if ($product->children) {
            $vsale_end = array();

            foreach ($product->children as $pvariable) {
                $vsale_end[] = (int)get_post_meta($pvariable, '_sale_price_dates_to', true);

                if (get_post_meta($pvariable, '_sale_price_dates_to', true)) {
                    $countdown = true;
                }
            }
            if ($countdown) {
                /* get the latest time */
                $vsale_end_date = max($vsale_end);
                $vsale_end_date = date('Y/m/d', $vsale_end_date);
                ?>
                <?php $product_countdown .= '<div class="box-timer"><div class="timer-grid" data-time="' . esc_attr($vsale_end_date) . '"></div></div>'; ?>
                <?php
            }
        }
        ?>
        <?php
    }
    echo $product_countdown;
}

add_action('woocommerce_single_product_summary', 'galio_product_countdown', 35);
add_action('get_galio_product_countdown', 'galio_product_countdown');

//Show buttons wishlist, compare, email on product page
function galio_product_buttons()
{
    global $product;
    ?>
    <?php if (class_exists('YITH_Woocompare') || class_exists('YITH_WCWL')) { ?>
    <div class="actions">
        <div class="action-buttons">
            <div class="add-to-links">
                <?php if (class_exists('YITH_Woocompare')) { ?>
                    <?php echo do_shortcode('[yith_compare_button]') ?>
                <?php }
                if (class_exists('YITH_WCWL')) { ?>
                    <?php echo preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')); ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
    <?php
}

add_action('woocommerce_single_product_summary', 'galio_product_buttons', 30);

//Project organize
remove_action('projects_before_single_project_summary', 'projects_template_single_title', 10);
add_action('projects_single_project_summary', 'projects_template_single_title', 5);
remove_action('projects_before_single_project_summary', 'projects_template_single_short_description', 20);
remove_action('projects_before_single_project_summary', 'projects_template_single_gallery', 40);
add_action('projects_single_project_gallery', 'projects_template_single_gallery', 40);
//projects list
remove_action('projects_loop_item', 'projects_template_loop_project_title', 20);

//Change search form
function galio_search_form($form)
{
    if (get_search_query() != '') {
        $search_str = get_search_query();
    } else {
        $search_str = esc_html__('Search...', 'galio');
    }

    $form = '<form role="search" method="get" id="blogsearchform" class="searchform" action="' . esc_url(home_url('/')) . '" >
	<div class="form-input">
		<input class="input_text" type="text" value="' . esc_attr($search_str) . '" name="s" id="search_input" />
		<button class="button" type="submit" id="blogsearchsubmit"><i class="fa fa-search"></i></button>
		<input type="hidden" name="post_type" value="post" />
		</div>
	</form>';
    $form .= '<script type="text/javascript">';
    $form .= 'jQuery(document).ready(function(){
		jQuery("#search_input").focus(function(){
			if(jQuery(this).val()=="' . esc_html__('Search...', 'galio') . '"){
				jQuery(this).val("");
			}
		});
		jQuery("#search_input").focusout(function(){
			if(jQuery(this).val()==""){
				jQuery(this).val("' . esc_html__('Search...', 'galio') . '");
			}
		});
		jQuery("#blogsearchsubmit").click(function(){
			if(jQuery("#search_input").val()=="' . esc_html__('Search...', 'galio') . '" || jQuery("#search_input").val()==""){
				jQuery("#search_input").focus();
				return false;
			}
		});
	});';
    $form .= '</script>';
    return $form;
}

add_filter('get_search_form', 'galio_search_form');

add_filter('woocommerce_breadcrumb_defaults', 'galio_woocommerce_breadcrumbs', 20, 0);
function galio_woocommerce_breadcrumbs()
{
    return array(
        'delimiter' => '<li class="separator"> &gt; </li>',
        'wrap_before' => '<ul id="breadcrumbs" class="breadcrumbs">',
        'wrap_after' => '</ul>',
        'before' => '<li class="item">',
        'after' => '</li>',
        'home' => _x('Главная', 'breadcrumb', 'galio'),
    );
}


function galio_limitStringByWord($string, $maxlength, $suffix = '')
{

    if (function_exists('mb_strlen')) {
        // use multibyte functions by Iysov
        if (mb_strlen($string) <= $maxlength) return $string;
        $string = mb_substr($string, 0, $maxlength);
        $index = mb_strrpos($string, ' ');
        if ($index === FALSE) {
            return $string;
        } else {
            return mb_substr($string, 0, $index) . $suffix;
        }
    } else { // original code here
        if (strlen($string) <= $maxlength) return $string;
        $string = substr($string, 0, $maxlength);
        $index = strrpos($string, ' ');
        if ($index === FALSE) {
            return $string;
        } else {
            return substr($string, 0, $index) . $suffix;
        }
    }
}

// Set up the content width value based on the theme's design and stylesheet.
if (!isset($content_width))
    $content_width = 625;


function galio_setup()
{
    /*
     * Makes galio Themes available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on VinaGecko, use a find and replace
     * to change 'galio' to the name of your theme in all the template files.
     */
    load_theme_textdomain('galio', get_template_directory() . '/languages');

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // This theme supports a variety of post formats.
    add_theme_support('post-formats', array('image', 'gallery', 'video', 'audio'));

    // Register menus
    register_nav_menu('primary', esc_html__('Primary Menu Digital', 'galio'));
    register_nav_menu('primary2', esc_html__('Primary Menu Fashion', 'galio'));
    register_nav_menu('top-menu', esc_html__('Top Menu', 'galio'));
    register_nav_menu('mobilemenu', esc_html__('Mobile Menu Digital', 'galio'));
    register_nav_menu('mobilemenu2', esc_html__('Mobile Menu Fashion', 'galio'));
    /*swallow code register Category Product*/
    register_nav_menu('mobilemenucategory', esc_html__('Mobile Category Product Digital', 'galio'));
    register_nav_menu('mobilemenucategory2', esc_html__('Mobile Category Product Fashion', 'galio'));
    register_nav_menu('category-product', esc_html__('Category Product Digital', 'galio'));
    register_nav_menu('category-product2', esc_html__('Category Product Fashion', 'galio'));

    /*
     * This theme supports custom background color and image,
     * and here we also set up the default background color.
     */
    add_theme_support('custom-background', array(
        'default-color' => 'e6e6e6',
    ));
    add_theme_support("custom-header", array(
        'default-color' => '',
    ));

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support('post-thumbnails');

    set_post_thumbnail_size(1170, 9999); // Unlimited height, soft crop
    add_image_size('galio-category-thumb', 870, 580, true); // (cropped)
    add_image_size('galio-post-thumb', 300, 200, true); // (cropped)
    add_image_size('galio-post-thumbwide', 570, 352, true); // (cropped)
}

add_action('after_setup_theme', 'galio_setup');

function galio_get_font_url()
{
    $font_url = '';

    /* translators: If there are characters in your language that are not supported
     * by Open Sans, translate this to 'off'. Do not translate into your own language.
     */
    if ('off' !== _x('on', 'Open Sans font: on or off', 'galio')) {
        $subsets = 'latin,latin-ext';

        /* translators: To add an additional Open Sans character subset specific to your language,
         * translate this to 'galio', 'cyrillic' or 'vietnamese'. Do not translate into your own language.
         */
        $subset = _x('no-subset', 'Open Sans font: add new subset (galio, cyrillic, vietnamese)', 'galio');

        if ('cyrillic' == $subset)
            $subsets .= ',cyrillic,cyrillic-ext';
        elseif ('galio' == $subset)
            $subsets .= ',galio,galio-ext';
        elseif ('vietnamese' == $subset)
            $subsets .= ',vietnamese';

        $protocol = is_ssl() ? 'https' : 'http';
        $query_args = array(
            'family' => 'Open+Sans:400italic,700italic,400,700',
            'subset' => $subsets,
        );
        $font_url = add_query_arg($query_args, "$protocol://fonts.googleapis.com/css");
    }

    return $font_url;
}

function galio_scripts_styles()
{
    global $wp_styles, $wp_scripts, $galio_options;

    /*
     * Adds JavaScript to pages with the comment form to support
     * sites with threaded comments (when in use).
    */

    if (is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');

    if (!is_admin()) {
        // Add Bootstrap JavaScript
        wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true);

        // Google Map JS
        wp_enqueue_script('google-js', 'http://maps.google.com/maps/api/js', array('jquery'), '3.2.0', true);

        // Add jQuery Cookie
        wp_enqueue_script('jquery-cookie', get_template_directory_uri() . '/js/jquery.cookie.js', array('jquery'), '1.4.1', true);

        // Add Fancybox
        wp_enqueue_script('jquery-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '2.1.5', true);
        wp_enqueue_style('jquery-fancybox-css', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css', array(), '2.1.5');
        wp_enqueue_script('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.js', array('jquery'), '1.0.5', true);
        wp_enqueue_style('jquery-fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.css', array(), '1.0.5');

        //Superfish
        wp_enqueue_script('superfish-js', get_template_directory_uri() . '/js/superfish/superfish.min.js', array('jquery'), '1.3.15', true);

        //Add Shuffle js
        wp_enqueue_script('modernizr-custom-js', get_template_directory_uri() . '/js/modernizr.custom.min.js', array('jquery'), '2.6.2', true);
        wp_enqueue_script('shuffle-js', get_template_directory_uri() . '/js/jquery.shuffle.min.js', array('jquery'), '3.0.0', true);

        // Add owl.carousel files
        wp_enqueue_script('owl.carousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'));
        wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css');
        wp_enqueue_style('owl.theme', get_template_directory_uri() . '/css/owl.theme.css');

        // Add jQuery countdown file
        wp_enqueue_script('countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array('jquery'), '2.0.4', true);

        // Add Plugin JS
        wp_enqueue_script('plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '20160115', true);


        // Add theme.js file
        wp_enqueue_script('galio-theme-js', get_template_directory_uri() . '/js/theme.js', array('jquery'), '20140826', true);
    }

    $font_url = galio_get_font_url();
    if (!empty($font_url))
        wp_enqueue_style('galio-fonts', esc_url_raw($font_url), array(), null);

    if (!is_admin()) {

        //Swallow Code Custom elements Visual Composer
        // Load Icon picker fonts:
        wp_enqueue_style('typicons', get_template_directory_uri() . '/css/typicons.min.css', array(), '4.9.2');
        wp_enqueue_style('openiconic', get_template_directory_uri() . '/css/vc_openiconic.min.css', array(), '4.9.2');
        wp_enqueue_style('linecons', get_template_directory_uri() . '/css/vc_linecons_icons.min.css', array(), '4.9.2');
        wp_enqueue_style('entypo', get_template_directory_uri() . '/css/vc_entypo.min.css', array(), '4.9.2');
        //End Swallow Code Custom elements Visual Composer

        // Loads our main stylesheet.
        wp_enqueue_style('galio-style', get_stylesheet_uri());

        wp_enqueue_style('elusive', get_template_directory_uri() . '/css/elusive-icons.min.css', array(), '2.0.0');

        // Load bootstrap css
        if (!is_rtl()) {
            wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.5');
        }

        // Load elegant css
        wp_enqueue_style('elegant-css', get_template_directory_uri() . '/css/elegant-style.css', array(), '1.0');

        // Load elegant css
        wp_enqueue_style('themify-style', get_template_directory_uri() . '/css/themify-icons.css', array(), '1.0');

        // Load fontawesome css
        wp_enqueue_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.2.0');

        if (is_rtl()) {
            wp_enqueue_style('galio-rtl', get_template_directory_uri() . '/css/bootstrap-rtl.min.css', array(), '3.3.2-rc1');
        }
    }
    // Compile Less to CSS

    // HieuJa get Preset Color Option
    $presetopt = galio_get_preset();
    // HieuJa end block

    if ($galio_options['bodyfont']['font-family']) {
        $bodyfont = $galio_options['bodyfont']['font-family'];
    } else {
        $bodyfont = '"Helvetica Neue",Helvetica,Arial,sans-serif';
    }

    if ($galio_options['enable_less']) {
        $themevariables = array(
            'body_font' => $bodyfont,
            'primary_color' => $galio_options['primary_color'],
            'text_color' => $galio_options['text_color'],
            'cart_color' => $galio_options['cart_color'],
            'top_bottom_color' => $galio_options['top_bottom_color'],
            'botom_color' => $galio_options['botom_color'],
            'footer_color' => $galio_options['footer_color'],
            'sale_color' => $galio_options['sale_color'],
            'featured_color' => $galio_options['featured_color'],
            'rate_color' => $galio_options['rate_color'],
            'presetopt' => $presetopt,
        );
        switch ($presetopt) {
            case 2:
                $themevariables['primary_color'] = $galio_options['primary2_color'];
                $themevariables['text_color'] = $galio_options['text2_color'];
                $themevariables['cart_color'] = $galio_options['cart2_color'];
                $themevariables['top_bottom_color'] = $galio_options['top_bottom2_color'];
                $themevariables['botom_color'] = $galio_options['botom2_color'];
                $themevariables['footer_color'] = $galio_options['footer2_color'];
                $themevariables['sale_color'] = $galio_options['sale2_color'];
                $themevariables['featured_color'] = $galio_options['featured2_color'];
                $themevariables['rate_color'] = $galio_options['rate2_color'];
                break;
            case 3:
                $themevariables['primary_color'] = $galio_options['primary3_color'];
                $themevariables['text_color'] = $galio_options['text3_color'];
                $themevariables['cart_color'] = $galio_options['cart3_color'];
                $themevariables['top_bottom_color'] = $galio_options['top_bottom3_color'];
                $themevariables['botom_color'] = $galio_options['botom3_color'];
                $themevariables['footer_color'] = $galio_options['footer3_color'];
                $themevariables['sale_color'] = $galio_options['sale3_color'];
                $themevariables['featured_color'] = $galio_options['featured3_color'];
                $themevariables['rate_color'] = $galio_options['rate3_color'];
                break;
            case 4:
                $themevariables['primary_color'] = $galio_options['primary4_color'];
                $themevariables['text_color'] = $galio_options['text4_color'];
                $themevariables['cart_color'] = $galio_options['cart4_color'];
                $themevariables['top_bottom_color'] = $galio_options['top_bottom4_color'];
                $themevariables['botom_color'] = $galio_options['botom4_color'];
                $themevariables['footer_color'] = $galio_options['footer4_color'];
                $themevariables['sale_color'] = $galio_options['sale4_color'];
                $themevariables['featured_color'] = $galio_options['featured4_color'];
                $themevariables['rate_color'] = $galio_options['rate4_color'];
                break;
        }
        if (function_exists('compileLessFile')) {
            compileLessFile('theme.less', 'theme' . $presetopt . '.css', $themevariables);
            compileLessFile('compare.less', 'compare' . $presetopt . '.css', $themevariables);
            compileLessFile('ie.less', 'ie' . $presetopt . '.css', $themevariables);
        }
    }

    if (!is_admin()) {
        if (isset($presetopt)) {
            // Load main theme css style
            wp_enqueue_style('galio-css', get_template_directory_uri() . '/css/theme' . $presetopt . '.css', array(), '1.0.0');
            //Compare CSS
            wp_enqueue_style('galio-css', get_template_directory_uri() . '/css/compare' . $presetopt . '.css', array(), '1.0.0');
            // Loads the Internet Explorer specific stylesheet.
            wp_enqueue_style('galio-ie', get_template_directory_uri() . '/css/ie' . $presetopt . '.css', array('galio-style'), '20152907');
        } else {
            // Load main theme css style
            wp_enqueue_style('galio-css', get_template_directory_uri() . '/css/theme1.css', array(), '1.0.0');
            //Compare CSS
            wp_enqueue_style('galio-css', get_template_directory_uri() . '/css/compare1.css', array(), '1.0.0');
            // Loads the Internet Explorer specific stylesheet.
            wp_enqueue_style('galio-ie', get_template_directory_uri() . '/css/ie1.css', array('galio-style'), '20152907');
        }
        $wp_styles->add_data('galio-ie', 'conditional', 'lte IE 9');
    }

    if ($galio_options['enable_sswitcher']) {
        // Add styleswitcher.js file
        wp_enqueue_script('galio-styleswitcher-js', get_template_directory_uri() . '/js/styleswitcher.js', array(), '20140826', true);
        // Load styleswitcher css style
        wp_enqueue_style('galio-styleswitcher-css', get_template_directory_uri() . '/css/styleswitcher.css', array(), '1.0.0');
    }
    if (is_rtl()) {
        wp_enqueue_style('galio-rtl', get_template_directory_uri() . '/rtl.css', array(), '1.0.0');
    }
}

add_action('wp_enqueue_scripts', 'galio_scripts_styles');

//Swallow Code Custom elements Visual Composer        
function galio_load_custom_wp_admin_style()
{
    wp_enqueue_style('vg_js_composer_icon', get_template_directory_uri() . '/css/vg_js_composer_icon.css', array(), '1.0.0');
}

add_action('admin_enqueue_scripts', 'galio_load_custom_wp_admin_style');
//End Swallow Code Custom elements Visual Composer

//Include
if (!class_exists('matalo_widgets') && file_exists(get_template_directory() . '/include/vinawidgets.php')) {
    require_once(get_template_directory() . '/include/vinawidgets.php');
}

/* Widgets list */
$vg_widgets = array(
    'vg_megamenu.php',
    'vg_woocarousel.php',
    'vg_postcarousel.php',
);

foreach ($vg_widgets as $widget) {
    require get_template_directory() . '/include/widgets/' . $widget;
}

//Include
if (!class_exists('galio_widgets') && file_exists(get_template_directory() . '/include/vinawidgets.php')) {
    require_once(get_template_directory() . '/include/vinawidgets.php');
}
if (file_exists(get_template_directory() . '/include/styleswitcher.php')) {
    require_once(get_template_directory() . '/include/styleswitcher.php');
}
if (file_exists(get_template_directory() . '/include/breadcrumbs.php')) {
    require_once(get_template_directory() . '/include/breadcrumbs.php');
}
if (file_exists(get_template_directory() . '/include/wooajax.php')) {
    require_once(get_template_directory() . '/include/wooajax.php');
}
if (file_exists(get_template_directory() . '/include/shortcodes.php')) {
    require_once(get_template_directory() . '/include/shortcodes.php');
}

function galio_mce_css($mce_css)
{
    $font_url = galio_get_font_url();

    if (empty($font_url))
        return $mce_css;

    if (!empty($mce_css))
        $mce_css .= ',';

    $mce_css .= esc_url_raw(str_replace(',', '%2C', $font_url));

    return $mce_css;
}

add_filter('mce_css', 'galio_mce_css');

/**
 * Filter the page menu arguments.
 *
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since VinaGecko 1.5
 */
function galio_page_menu_args($args)
{
    if (!isset($args['show_home']))
        $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'galio_page_menu_args');

/**
 * Register sidebars.
 *
 * Registers our main widget area and the front page widget areas.
 *
 * @since VinaGecko 1.5
 */
function galio_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('VG Blog Sidebar', 'galio'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Sidebar on blog page', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Top Header', 'galio'),
        'id' => 'sidebar-vg-top-header',
        'description' => esc_html__('Sidebar on Top Header', 'galio'),
        'before_widget' => '<div id="%1$s" class="widget col-md-6 col-sm-12 col-xs-12 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Layout 1,2 Left Main Menu', 'galio'),
        'id' => 'sidebar-vg-left-main-menu1',
        'description' => esc_html__('Sidebar Layout 1,2 Left Main Menu', 'galio'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Layout 3 Left Main Menu', 'galio'),
        'id' => 'sidebar-vg-left-main-menu2',
        'description' => esc_html__('Sidebar Layout 3 Left Main Menu', 'galio'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Layout 4 Left Main Menu', 'galio'),
        'id' => 'sidebar-vg-left-main-menu3',
        'description' => esc_html__('Sidebar Layout 4 Left Main Menu', 'galio'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => esc_html__('VG Category Sidebar', 'galio'),
        'id' => 'sidebar-category',
        'description' => esc_html__('Sidebar on product category page', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Layout 1,2 Product Sidebar', 'galio'),
        'id' => 'sidebar-product',
        'description' => esc_html__('VG Layout 1,2 Sidebar on product page', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Layout 3,4 Product Sidebar', 'galio'),
        'id' => 'sidebar-product2',
        'description' => esc_html__('VG Layout 3,4 Sidebar on product page', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget vg-widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Pages Sidebar', 'galio'),
        'id' => 'sidebar-page',
        'description' => esc_html__('Sidebar on content pages', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => esc_html__('VG Bot Main Sidebar', 'galio'),
        'id' => 'sidebar-botmain',
        'description' => esc_html__('Bot Main Sidebar on content pages', 'galio'),
        'before_widget' => '<aside id="%1$s" class="widget col-md-12 col-sm-12 col-xs-12 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<div class="vg-title widget-title"><h3>',
        'after_title' => '</h3></div>',
    ));
    register_sidebar(array(
        'name' => esc_html__('VG Widget Bottom', 'galio'),
        'id' => 'bottom',
        'class' => 'bottom',
        'description' => esc_html__('Widget on bottom', 'galio'),
        'before_widget' => '<div class="widget vg-bottom-menu text-left col-lg-3 col-md-3 col-sm-6 col-xs-12">',
        'after_widget' => '</div>',
        'before_title' => '<div class="vg-title bottom-static-title"><h3>',
        'after_title' => '</h3></div>',
    ));
}

add_action('widgets_init', 'galio_widgets_init');


if (!function_exists('galio_content_nav')) :
    /**
     * Displays navigation to next/previous pages when applicable.
     *
     * @since VinaGecko 1.5
     */
    function galio_content_nav($html_id)
    {
        global $wp_query;

        $html_id = esc_attr($html_id);

        if ($wp_query->max_num_pages > 1) : ?>
            <nav id="<?php echo esc_attr($html_id); ?>" class="navigation" role="navigation">
                <h3 class="assistive-text"><?php esc_html_e('Post navigation', 'galio'); ?></h3>
                <div
                    class="nav-previous"><?php next_posts_link(wp_kses(__('<span class="meta-nav">&larr;</span> Older posts', 'galio'), array('span' => array('class' => array())))); ?></div>
                <div
                    class="nav-next"><?php previous_posts_link(wp_kses(__('Newer posts <span class="meta-nav">&rarr;</span>', 'galio'), array('span' => array('class' => array())))); ?></div>
            </nav><!-- #<?php echo esc_attr($html_id); ?> .navigation -->
        <?php endif;
    }
endif;

if (!function_exists('galio_pagination')) :
    /* Pagination */
    function galio_pagination()
    {
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        echo paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
            'prev_text' => wp_kses(__('<i class="fa fa-chevron-left"></i>', 'galio'), array('i' => array('class' => array()))),
            'next_text' => wp_kses(__('<i class="fa fa-chevron-right"></i>', 'galio'), array('i' => array('class' => array()))),
        ));
    }
endif;

if (!function_exists('galio_entry_meta')) :
    function galio_entry_meta()
    {
        // Translators: used between list items, there is a space after the comma.
        $categories_list = '<span class="categories-list"><i class="fa fa-folder-o"></i>' . get_the_category_list(esc_html__(', ', 'galio')) . '</span>';

        $date = sprintf('<span class="posted-on"><i class="fa fa-calendar"></i><a href="' . get_permalink() . '" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
            esc_url(get_permalink()),
            esc_attr(get_the_time()),
            esc_attr(get_the_date('c')),
            esc_html(get_the_date())
        );

        $author = sprintf('<span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
            esc_url(get_author_posts_url(get_the_author_meta('ID'))),
            esc_attr(sprintf(esc_html__('View all posts by %s', 'galio'), get_the_author())),
            get_the_author()
        );

        $num_comments = (int)get_comments_number();
        $write_comments = '';
        if (comments_open()) {
            if ($num_comments == 0) {
                $comments = esc_html__('0 comments', 'galio');
            } elseif ($num_comments > 1) {
                $comments = $num_comments . esc_html__(' comments', 'galio');
            } else {
                $comments = esc_html__('1 comment', 'galio');
            }
            $write_comments = '<span class="comments-link"><i class="fa fa-comments-o"></i><a href="' . get_comments_link() . '" class="link-comment">' . $comments . '</a></span>';
        }

        // Translators: 1 is author's name, 2 is date, 3 is the tags and 4 is comments.

        $utility_text = wp_kses(__('%1$s%2$s%3$s%4$s', 'galio'), array());
        printf($utility_text, $author, $date, $categories_list, $write_comments);
    }

endif;

if (!function_exists('galio_entry_meta_tags')) :
    function galio_entry_meta_tags()
    {
        // Translators: used between list items, there is a space after the comma.
        $tag_list = '<div class="entry-tags"><span> ' . esc_html__('Tags: ', 'galio') . ' </span>' . get_the_tag_list('', esc_html__(', ', 'galio')) . '</div>';

        printf($tag_list);
    }
endif;

function galio_entry_meta_small()
{

    $date = sprintf('<span class="posted-on"><i class="fa fa-calendar"></i><a href="' . get_permalink() . '" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
        esc_url(get_permalink()),
        esc_attr(get_the_time()),
        esc_attr(get_the_date('c')),
        esc_html(get_the_date())
    );

    $author = sprintf('<span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
        esc_url(get_author_posts_url(get_the_author_meta('ID'))),
        esc_attr(sprintf(esc_html__('View all posts by %s', 'galio'), get_the_author())),
        get_the_author()
    );

    $num_comments = (int)get_comments_number();
    $write_comments = '';
    if (comments_open()) {
        if ($num_comments == 0) {
            $comments = esc_html__('0 comments', 'galio');
        } elseif ($num_comments > 1) {
            $comments = $num_comments . esc_html__(' comments', 'galio');
        } else {
            $comments = esc_html__('1 comment', 'galio');
        }
        $write_comments = '<span class="comments-link"><i class="fa fa-comments-o"></i><a href="' . get_comments_link() . '" class="link-comment">' . $comments . '</a></span>';
    }

    $utility_text = wp_kses(__('<div class="entry-meta">%1$s%2$s%3$s</div>', 'galio'), array('div' => array('class' => array())));

    printf($utility_text, $author, $date, $write_comments);
}

function galio_add_meta_box()
{

    $screens = array('post');

    foreach ($screens as $screen) {

        add_meta_box(
            'galio_post_intro_section',
            esc_html__('Post featured content', 'galio'),
            'galio_meta_box_callback',
            $screen
        );
    }
}

add_action('add_meta_boxes', 'galio_add_meta_box');

function galio_meta_box_callback($post)
{

    // Add an nonce field so we can check for it later.
    wp_nonce_field('galio_meta_box', 'galio_meta_box_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $value = get_post_meta($post->ID, '_galio_meta_value_key', true);

    echo '<label for="galio_post_intro">';
    esc_html_e('This content will be used to replace the featured image, use shortcode here', 'galio');
    echo '</label><br />';
    //echo '<textarea id="galio_post_intro" name="galio_post_intro" rows="5" cols="50" />' . esc_attr( $value ) . '</textarea>';
    wp_editor($value, 'galio_post_intro', $settings = array());


}

function galio_save_meta_box_data($post_id)
{

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if (!isset($_POST['galio_meta_box_nonce'])) {
        return;
    }

    // Verify that the nonce is valid.
    if (!wp_verify_nonce($_POST['galio_meta_box_nonce'], 'galio_meta_box')) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Check the user's permissions.
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {

        if (!current_user_can('edit_page', $post_id)) {
            return;
        }

    } else {

        if (!current_user_can('edit_post', $post_id)) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Make sure that it is set.
    if (!isset($_POST['galio_post_intro'])) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field($_POST['galio_post_intro']);

    // Update the meta field in the database.
    update_post_meta($post_id, '_galio_meta_value_key', $my_data);
}

add_action('save_post', 'galio_save_meta_box_data');

if (!function_exists('galio_comment')) :
    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own galio_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since VinaGecko 1.5
     */
function galio_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
switch ($comment->comment_type) :
    case 'pingback' :
case 'trackback' :
    // Display trackbacks differently than normal comments.
    ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
    <p><?php esc_html_e('Pingback:', 'galio'); ?><?php comment_author_link(); ?><?php edit_comment_link(esc_html__('(Edit)', 'galio'), '<span class="edit-link">', '</span>'); ?></p>
    <?php
    break;
default :
    // Proceed with normal comments.
    global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <div class="comment-avatar">
                <?php echo get_avatar($comment, 50); ?>
            </div>
            <div class="comment-info">
                <header class="comment-meta comment-author vcard">
                    <?php

                    printf('<cite><b class="fn">%1$s</b> %2$s</cite>',
                        get_comment_author_link(),
                        // If current post author is also comment author, make it known visually.
                        ($comment->user_id === $post->post_author) ? '<span>' . esc_html__('Post author', 'galio') . '</span>' : ''
                    );
                    printf('<time datetime="%1$s">%2$s</time>',
                        get_comment_time('c'),
                        /* translators: 1: date, 2: time */
                        sprintf(esc_html__('%1$s at %2$s', 'galio'), get_comment_date(), get_comment_time())
                    );
                    ?>
                    <div class="reply">
                        <?php comment_reply_link(array_merge($args, array('reply_text' => esc_html__('Reply', 'galio'), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                    </div><!-- .reply -->
                </header><!-- .comment-meta -->
                <?php if ('0' == $comment->comment_approved) : ?>
                    <p class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'galio'); ?></p>
                <?php endif; ?>

                <section class="comment-content comment">
                    <?php comment_text(); ?>
                    <?php edit_comment_link(esc_html__('Edit', 'galio'), '<p class="edit-link">', '</p>'); ?>
                </section><!-- .comment-content -->
            </div>
        </article><!-- #comment-## -->
        <?php
        break;
        endswitch; // end comment_type check
        }
        endif;
        if (!function_exists('before_comment_fields') && !function_exists('after_comment_fields')) :
//Change comment form
            function galio_before_comment_fields()
            {
                echo '<div class="comment-input">';
            }

            add_action('comment_form_before_fields', 'galio_before_comment_fields');

            function galio_after_comment_fields()
            {
                echo '</div>';
            }

            add_action('comment_form_after_fields', 'galio_after_comment_fields');

        endif;

        function galio_customize_register($wp_customize)
        {
            $wp_customize->get_setting('blogname')->transport = 'postMessage';
            $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
            $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
        }

        add_action('customize_register', 'galio_customize_register');

        /**
         * Enqueue Javascript postMessage handlers for the Customizer.
         *
         * Binds JS handlers to make the Customizer preview reload changes asynchronously.
         *
         * @since VinaGecko 1.5
         */

        add_action('wp_enqueue_scripts', 'galio_wcqi_enqueue_polyfill');
        function galio_wcqi_enqueue_polyfill()
        {
            wp_enqueue_script('wcqi-number-polyfill');
        }

        /* Remove Redux Demo Link */
        function galio_removeDemoModeLink()
        {
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
            }
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
            }
        }

        add_action('init', 'galio_removeDemoModeLink');

        // HieuJa add specific class
        function galio_add_query_vars_filter($vars)
        {
            $vars[] = "ilayout";
            $vars[] = "preset";
            return $vars;
        }

        add_filter('query_vars', 'galio_add_query_vars_filter');

        // Get value from URL and set Cookie, Class Subffix
        function galio_body_class($classes)
        {
            global $galio_options;

            // Set Class Layout for Body Tag
            $classes[] = galio_get_layout();

            // Set Class Preset Color for Body Tag
            $classes[] = "preset-" . galio_get_preset();

            return $classes;
        }

        add_filter('body_class', 'galio_body_class');

        // Override get_header function
        function galio_get_header()
        {
            // Set Cookie for Layout and Preset Color
            galio_set_layout_preset_color();

            // Reset Layout and Preset Color
            galio_reset_layout_preset_color();

            // Get Header Name
            $header = galio_get_layout();

            get_header($header);
        }

        // Override get_footer function
        function galio_get_footer()
        {
            // Get Footer Name
            $footer = galio_get_layout();

            get_footer($footer);
        }

        // HieuJa get Layout
        function galio_get_layout()
        {
            global $galio_options;

            $ilayout = get_query_var('ilayout', '');
            if (!empty($ilayout)) {
                $layout = $ilayout;
            } else {
                $pageLayout = isset($_COOKIE['page-layout']) ? $_COOKIE['page-layout'] : "";
                $defaultLayout = isset($galio_options['page_layout']) ? $galio_options['page_layout'] : "layout-1";
                $layout = (!empty($pageLayout)) ? $pageLayout : $defaultLayout;
            }

            return $layout;
        }

        // HieuJa get Preset Color
        function galio_get_preset()
        {
            global $galio_options;

            $preset = get_query_var('preset', '');
            if (!empty($preset)) {
                $presetColor = $preset;
            } else {
                $presetColor = isset($_COOKIE['preset-color']) ? $_COOKIE['preset-color'] : "";
                $defaultColor = isset($galio_options['preset_option']) ? $galio_options['preset_option'] : "1";
                $presetColor = ((!empty($presetColor)) ? $presetColor : $defaultColor);

            }

            return $presetColor;
        }

        // HieuJa set Cookie for Layout and Preset Color
        function galio_set_layout_preset_color()
        {
            // Set Layout Style
            $ilayout = get_query_var('ilayout', '');
            if (!empty($ilayout)) {
                setcookie('page-layout', $ilayout, strtotime('+1 day'), '/');
            }

            // Set Preset Color
            $preset = get_query_var('preset', '');
            if (!empty($preset)) {
                setcookie('preset-color', $preset, strtotime('+1 day'), '/');
            }

            return true;
        }

        // HieuJa reset Layout Option and Preset Color
        function galio_reset_layout_preset_color()
        {
            global $galio_options;

            $requestURI = $_SERVER['REQUEST_URI'];
            $homeURL = home_url('/');
            $isHomePage = strpos($homeURL, $requestURI);

            if ($requestURI == '/' || $isHomePage !== false) {
                $pageLayout = isset($_COOKIE['page-layout']) ? $_COOKIE['page-layout'] : $galio_options['page_layout'];
                $presetColor = isset($_COOKIE['preset-color']) ? $_COOKIE['preset-color'] : $galio_options['preset_option'];
                if ($pageLayout != $galio_options['page_layout']
                    || $presetColor != $galio_options['preset_option']
                ) {
                    setcookie('page-layout', $galio_options['page_layout'], strtotime('+1 day'), '/');
                    setcookie('preset-color', $galio_options['preset_option'], strtotime('+1 day'), '/');
                    wp_redirect(home_url());
                    exit;
                }
            }
        }

        add_action('init', 'galio_reset_layout_preset_color');

        // HieuJa get global variables
        function galio_get_global_variables($variable = 'galio_options')
        {
            global $woocommerce, $project, $projects, $galio_productrows, $galio_options, $galio_productsfound, $product, $woocommerce_loop, $projects_loop, $post, $galio_projectrows, $galio_projectsfound, $galio_secondimage, $wpdb, $wp_query, $is_IE;

            switch ($variable) {
                case "galio_options":
                    return $galio_options;
                    break;
                case "product":
                    return $product;
                    break;
                case "woocommerce":
                    return $woocommerce;
                    break;
                case "project":
                    return $project;
                    break;
                case "projects":
                    return $projects;
                    break;
                case "galio_productrows":
                    return $galio_productrows;
                    break;
                case "galio_productsfound":
                    return $galio_productsfound;
                    break;
                case "woocommerce_loop":
                    return $woocommerce_loop;
                    break;
                case "projects_loop":
                    return $projects_loop;
                    break;
                case "post":
                    return $post;
                    break;
                case "galio_projectrows":
                    return $galio_projectrows;
                    break;
                case "galio_projectsfound":
                    return $galio_projectsfound;
                    break;
                case "galio_secondimage":
                    return $galio_secondimage;
                    break;
                case "wpdb":
                    return $wpdb;
                    break;
                case "wp_query":
                    return $wp_query;
                    break;
                case "is_IE":
                    return $is_IE;
                    break;
            }

            return false;
        }

        //swallow code sale and featured %
        add_filter('woocommerce_featured_flash', 'galio_change_featured_flash');
        function galio_change_featured_flash()
        {
            global $galio_options;
            return '<div class="vgwc-label vgwc-featured"><span>' . $galio_options['featured_label_custom'] . '</span></div>';
        }

        function galio_woothemes_testimonials_html($html, $query, $args)
        {
            $html = str_replace('<div class="testimonials-list">', '<div class="testimonials-list">', $html);
            return $html;
        }

        add_filter('woothemes_testimonials_html', 'galio_woothemes_testimonials_html', 10, 3);

        add_filter('woocommerce_sale_flash', 'galio_change_on_sale_flash');
        function galio_change_on_sale_flash()
        {
            global $product, $post, $galio_options;
            $sale = '';
            $format = $galio_options['sale_label'];

            if (!empty($format)) {
                if ($format == 'custom') {
                    $format = $galio_options['sale_label_custom'];
                }
                $priceDiff = 0;
                $percentDiff = 0;
                $regularPrice = '';
                $salePrice = '';
                if (!empty($product)) {
                    $salePrice = get_post_meta($product->id, '_price', true);
                    $regularPrice = get_post_meta($product->id, '_regular_price', true);

                    if ($product->get_children()) {
                        foreach ($product->get_children() as $child_id) {
                            $all_prices[] = get_post_meta($child_id, '_price', true);
                            $all_prices1[] = get_post_meta($child_id, '_regular_price', true);
                            $all_prices2[] = get_post_meta($child_id, '_sale_price', true);
                            if (get_post_meta($child_id, '_price', true) == $salePrice) {
                                $regularPrice = get_post_meta($child_id, '_regular_price', true);
                            }
                        }
                    }
                }

                if (!empty($regularPrice) && !empty($salePrice) && $regularPrice > $salePrice) {
                    $priceDiff = $regularPrice - $salePrice;
                    $percentDiff = round($priceDiff / $regularPrice * 100);

                    $parsed = str_replace('{price-diff}', number_format((float)$priceDiff, 2, '.', ''), $format);
                    $parsed = str_replace('{percent-diff}', $percentDiff, $parsed);
                    if ($product->is_featured()) {
                        $sale = '<div class="vgwc-label vgwc-onsale sale-featured"><span>' . $parsed . '</span></div>';
                    } else {
                        $sale = '<div class="vgwc-label vgwc-onsale"><span>' . $parsed . '</span></div>';
                    }
                }
            }
            return $sale;
        }

        function galio_max_menu_item () {
        global $galio_options;
        ?>
        <script type="text/javascript">
            var vg_max_menu = false;
            <?php if(($galio_options['show_menu_max'] == true) && isset($galio_options['show_menu_max'])) { ?>
            vg_max_menu = true;
            var title_more_categories = "<?php echo $galio_options['title_more_categories']; ?>";
            var title_close_menu = "<?php echo $galio_options['title_close_menu']; ?>";
            var menu_max_number = <?php echo $galio_options['menu_max_number'] + 1; ?>;
            <?php } else { ?>
            vg_max_menu = false;
            <?php } ?>
        </script>
    <?php
}
add_action('wp_head', 'galio_max_menu_item');