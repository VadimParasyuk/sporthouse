<?php

/**

 * @version    1.6

 * @package    VG Galio

 * @author     VinaGecko <support@vinagecko.com>

 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.

 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html

 *

 * Websites: http://vinagecko.com

 */

$galio_options  = galio_get_global_variables(); 



// HieuJa get Preset Color Option

$presetopt = galio_get_preset();

// HieuJa end block



?>

			<?php if (is_active_sidebar('sidebar-botmain')) : ?>

			<div class="bot-main-wrapper">

				<div class="container">

					<div class="row">

						<?php dynamic_sidebar('sidebar-botmain'); ?>

					</div>

				</div>

			</div>

			<?php endif; ?>



			<?php if((isset($galio_options['enable_brands']) && $galio_options['enable_brands']) && do_shortcode("[ourbrands]")) : ?>

				<div class="vg-brand-slider-wrapper">

					<div class="container">

						<div class="row">

							<div class="our-brands col-sm-12 col-xs-12 widget">

								<div class="wpb_wrapper">

									<?php if(do_shortcode("[ourbrands]")) : ?>

										<?php echo do_shortcode("[ourbrands title='". $galio_options['brands_title']. "' add_show_icon='yes' type='fontawesome' icon_fontawesome='". $galio_options['brands_icon'] ."' ]"); ?>

									<?php endif; ?>

								</div>

							</div>

						</div>

					</div>

				</div>

			<?php endif; ?>



			

			

			<?php if($galio_options['newsletter_show'] || $galio_options['social_show']) : ?>

			<div class="top-bottom-wrapper">

				<div class="container">

					<div class="row">

						<?php if($galio_options['newsletter_show']) : ?>

						<div class="vg-newsletter <?php echo ($galio_options['social_show']) ? 'col-md-9 col-sm-12 col-xs-12' : 'col-md-12 col-sm-12 col-xs-12'; ?>">

							<div class="newsletter-container">

								<div class="header-newsletter">

									<h3><?php echo esc_html($galio_options['newsletter_title']); ?></h3>

									<p class="des"><?php echo $galio_options['newsletter_des']; ?></p>

								</div>

								<?php 

									echo do_shortcode("[wysija_form id='" . $galio_options['newsletter_form'] . "']");

								?>

							</div>		

						</div>		

						<?php endif; ?>

						

						<?php if($galio_options['social_show']) : ?>

						<div class="vg-social <?php echo ($galio_options['newsletter_show']) ? 'col-md-3 col-sm-12 col-xs-12' : 'col-md-12 col-sm-12 col-xs-12'; ?>">

						<!-- Social -->

						<?php

								echo '<ul class="social-icons">';

								foreach($galio_options['ftsocial_icons'] as $key=>$value) {

									if($value!=''){

										if($key=='vimeo'){

											echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';

										} else {

											echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';

										}

									}

								}

								echo '</ul>';

							?>

						</div>

						<?php endif; ?>

					</div>

				</div>

			</div>

			<?php endif; ?>

			

			

			

			

			<!-- Bottom -->

			<?php if (is_active_sidebar('bottom') ) : ?>

			<div class="bottom-wrapper">

				<div class="container">

					<div class="row">

						

						<?php if (is_active_sidebar('bottom')) : ?>

							<?php

								dynamic_sidebar( 'bottom' );

							?>

						<?php endif;?>

			

						

					</div>

				</div>

			</div>

			<?php endif; ?>

			

			<div class="footer-wrapper">

				<div class="container">

					<div class="row">

						<div class="container-fluid">

							<?php if($galio_options['copyright_show']) { ?>

							<div class="col-lg-<?php echo ($galio_options['footer_payment']['url']) ? '6' : '12 text-center' ; ?> col-md-<?php echo ($galio_options['footer_payment']['url']) ? '6' : '12 text-center' ; ?> col-sm-<?php echo ($galio_options['footer_payment']['url']) ? '6' : '12 text-center' ; ?> col-xs-12">

								<div class="copyright">

									<?php echo galio_copyright(); ?>

								</div>

							</div>

							<?php } ?>

							<?php if(!empty($galio_options['footer_payment']['url'])){ ?>

							<div class="col-payment col-lg-<?php echo ($galio_options['copyright_show']) ? '6' : '12' ; ?> col-md-<?php echo ($galio_options['copyright_show']) ? '6' : '12' ; ?> col-sm-<?php echo ($galio_options['copyright_show']) ? '6' : '12' ; ?> col-xs-12 <?php echo ($galio_options['copyright_show']) ? 'text-right' : 'text-center' ; ?>">

								<div class="vg-payment">

									<img class="payment" src="<?php echo esc_url($galio_options['footer_payment']['url']); ?>" alt="" />

								</div>

							</div>

							<?php } ?>

						</div>

					</div>

				</div>

			</div>

			

	</div><!-- .wrapper -->

	<div class="to-top"><i class="fa fa-chevron-up"></i></div>



	<!--[if lt IE 9]>

	<script src="<?php echo get_template_directory_uri(); ?>/js/ie8.js" type="text/javascript"></script>

	<![endif]-->

	<?php wp_footer(); ?>

<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-84070854-1', 'auto');

  ga('send', 'pageview');



</script>



<!-- <script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script> -->



<!-- VK Widget

<div id="vk_community_messages"></div>

<script type="text/javascript">

VK.Widgets.CommunityMessages("vk_community_messages", 96509629, {shown: "1"});

</script>

 -->



<!--BEGIN JIVOSITE CODE {literal}

<script type="text/javascript">

(function(){ var widget_id = '55sLfTrGqd';var d=document;var w=window;function l(){

var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();

</script>

{/literal} END JIVOSITE CODE -->



<script type="text/javascript">

var fca_code = '7140bb33c5c99d848b11eba6c624494b';

(function() {

var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.charset = 'utf-8';

po.src = '//ua.cdn.fastcallagent.com/fca.min.js?_='+Date.now();

var s = document.getElementsByTagName('script')[0];

if (s) { s.parentNode.insertBefore(po, s); }

else { s = document.getElementsByTagName('head')[0]; s.appendChild(po); }

})();

</script>



<!-- Yandex.Metrika counter -->

<script type="text/javascript">

(function (d, w, c) {

(w[c] = w[c] || []).push(function() {

try {

w.yaCounter41134889 = new Ya.Metrika({

id:41134889,

clickmap:true,

trackLinks:true,

accurateTrackBounce:true

});

} catch(e) { }

});



var n = d.getElementsByTagName("script")[0],

s = d.createElement("script"),

f = function () { n.parentNode.insertBefore(s, n); };

s.type = "text/javascript";

s.async = true;

s.src = "https://mc.yandex.ru/metrika/watch.js";;;



if (w.opera == "[object Opera]") {

d.addEventListener("DOMContentLoaded", f, false);

} else { f(); }

})(document, window, "yandex_metrika_callbacks");



</script>

<!-- /Yandex.Metrika counter -->





<div style="display:none" class="fancybox-hidden">

<div id="contact_form_pop">

<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>

</div>

</div>


<script type="text/javascript">
    (function () {

        document.ondragstart = noselect;
        document.onselectstart = noselect;
        document.oncontextmenu = noselect;

        function noselect(event) {
            event.preventDefault();
            return false;
        }
    }());
</script>


</body>

</html>