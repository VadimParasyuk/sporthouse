<?php
/**
 * @version    1.6
 * @package    VG Galio
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
$galio_options  = galio_get_global_variables(); 

galio_get_header();
?>
<?php 
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = 'blog-sidebar';

	$bloglayout = $_GET['layout'];
	switch($bloglayout) {
		case 'nosidebar':
			$blogclass = 'blog-nosidebar';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		case 'fullwidth':
			$blogclass = 'blog-fullwidth';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		default:
			$blogclass = 'blog-sidebar';
			$blogcolclass = 9;
	}
}
else {
	$blogsidebar = 'right';
	if(isset($galio_options['sidebarblog_pos']) && $galio_options['sidebarblog_pos']!=''){
		$blogsidebar = $galio_options['sidebarblog_pos'];
	}
	if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
		$blogsidebar = $_GET['sidebar'];
	}

	switch($blogsidebar) {
		case 'nosidebar':
			$blogclass = 'blog-nosidebar';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		case 'fullwidth':
			$blogclass = 'blog-fullwidth';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		default:
			$blogclass = 'blog-sidebar';
			$blogcolclass = 9;
	}
}

?>

<div class="main-container default-page page-category">
	<div class="container">
		
		<?php galio_breadcrumb(); ?>
		
		<div class="row">
			<?php if($blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo  (is_active_sidebar('sidebar-1')) ? 'col-md-'. esc_attr($blogcolclass) : 'col-md-12'; ?>">
				<div class="page-content blog-page <?php echo esc_attr($blogclass); if($blogsidebar=='left') {echo ' left-sidebar'; } if($blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if (have_posts()) : ?>
						<header class="archive-header">
							<h1 class="archive-title"><?php
								if (is_day()) :
									printf(esc_html__('Daily Archives: %s', 'galio'), '<span>' . get_the_date() . '</span>');
								elseif (is_month()) :
									printf(esc_html__('Monthly Archives: %s', 'galio'), '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'galio')) . '</span>');
								elseif (is_year()) :
									printf(esc_html__('Yearly Archives: %s', 'galio'), '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'galio')) . '</span>');
								else :
									esc_html_e('Archives', 'galio');
								endif;
							?></h1>
						</header><!-- .archive-header -->

						<?php
						/* Start the Loop */
						while (have_posts()) : the_post();

							/* Include the post format-specific template for the content. If you want to
							 * this in a child theme then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part('content', get_post_format());

						endwhile;
						?>
						
						<div class="pagination">
							<?php galio_pagination(); ?>
						</div>
						
					<?php else : ?>
						<?php get_template_part('content', 'none'); ?>
					<?php endif; ?>
				</div>
			</div>
			
			<?php if($blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php galio_get_footer(); ?>