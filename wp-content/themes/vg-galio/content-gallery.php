<?php
/**
 * @version    1.6
 * @package    VG Galio
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */
?>
<?php $galio_options  = galio_get_global_variables();  ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content-full">
		<?php if (! post_password_required() && ! is_attachment()) : ?>
		<div class="post-thumbnail">
			<?php echo do_shortcode(get_post_meta($post->ID, '_galio_meta_value_key', true)); ?>
		</div>
		<?php endif; ?>
		
		<div class="postinfo-wrapper">
		
			<?php if (is_single()) : ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php else : ?>
				<h2 class="entry-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h2>
			<?php endif; ?>
		
			<?php if (is_single()) : ?>
				<div class="entry-meta">
					<?php galio_entry_meta(); ?>
				</div>
			<?php endif; ?>
				
			<?php if (!is_single()) : ?>
				<div class="entry-meta-small">
					<?php if (is_sticky() && is_home() && ! is_paged()) : ?>
						<div class="entry-meta">
							<?php galio_entry_meta(); ?>
						</div>
					<?php else : ?>
						<?php galio_entry_meta_small(); ?>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			
			<div class="post-info">
				
				<?php if (is_single()) : ?>
					<div class="entry-content">
						<?php the_content(wp_kses(__('Continue reading <span class="meta-nav">&rarr;</span>', 'galio'), array('span' => array()))); ?>
						<?php wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'galio'), 'after' => '</div>', 'pagelink' => '<span>%</span>')); ?>
						
					</div>
					
					<div class="entry-footer row">
						<div class="col-sm-<?php echo (has_action('vg_social_share')) ? '6' : '12' ; ?> col-xs-12">
							<?php galio_entry_meta_tags(); ?>
						</div>
						
						<?php if(has_action('vg_social_share')) : ?>
						<div class="col-sm-6 col-xs-12 text-right" style="padding-top: 10px;">
							<?php do_action('vg_social_share'); // Sharing plugins can hook into here ?>
						</div>
						<?php endif; ?>
						
					</div>
					
				<?php else : ?>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
						<a class="readmore" href="<?php the_permalink(); ?>"><?php esc_html_e('Read more', 'galio');  ?></a>
					</div>
				<?php endif; ?>
				
				<?php if (is_single() && $galio_options['galio_show_author']) : ?>
					<div class="author-info">
						<div class="author-avatar">
							<?php
							$author_bio_avatar_size = apply_filters('galio_author_bio_avatar_size', 68);
							echo get_avatar(get_the_author_meta('user_email'), $author_bio_avatar_size);
							?>
						</div>
						<div class="author-description">
							<h2><?php esc_html_e('About the Author:', 'galio');?>
							<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" rel="author"><?php printf('%s',get_the_author()); ?></a>
							</h2>
							<p><?php the_author_meta('description'); ?></p>
						</div>
					</div>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</article>