<?php
/**
 * Woocommerce Compare page
 *
 * @author Your Inspiration Themes
 * @package YITH Woocommerce Compare
 * @version 1.1.4
 */

// remove the style of woocommerce
if( defined('WOOCOMMERCE_USE_CSS') && WOOCOMMERCE_USE_CSS ) wp_dequeue_style('woocommerce_frontend_styles');

$is_iframe = (bool)( isset( $_REQUEST['iframe'] ) && $_REQUEST['iframe'] );

wp_enqueue_script( 'jquery-fixedheadertable', YITH_WOOCOMPARE_ASSETS_URL . '/js/jquery.dataTables.min.js', array('jquery'), '1.3', true );
wp_enqueue_script( 'jquery-fixedcolumns', YITH_WOOCOMPARE_ASSETS_URL . '/js/FixedColumns.min.js', array('jquery', 'jquery-fixedheadertable'), '1.3', true );

$widths = array();
foreach( $products as $product ) $widths[] = '{ "sWidth": "205px", resizeable:true }';

$table_text = get_option( 'yith_woocompare_table_text' );
yit_wpml_register_string( 'Plugins', 'plugin_yit_compare_table_text', $table_text );
$localized_table_text = yit_wpml_string_translate( 'Plugins', 'plugin_yit_compare_table_text', $table_text );

?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" class="ie"<?php language_attributes() ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" class="ie"<?php language_attributes() ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" class="ie"<?php language_attributes() ?>>
<![endif]-->
<!--[if IE 9]>
<html id="ie9" class="ie"<?php language_attributes() ?>>
<![endif]-->
<!--[if gt IE 9]>
<html class="ie"<?php language_attributes() ?>>
<![endif]-->
<!--[if !IE]>
<html <?php language_attributes() ?>>
<![endif]-->

<!-- START HEAD -->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
   <?php
	
		function galio_compare_css () {
			wp_enqueue_style( 'style-compare', YITH_WOOCOMPARE_ASSETS_URL . '/css/style.css' );
			wp_enqueue_style( 'jquery-colorbox', YITH_WOOCOMPARE_ASSETS_URL . '/css/colorbox.css' );
			wp_enqueue_style( 'jquery-dataTables', YITH_WOOCOMPARE_ASSETS_URL . '/css/jquery.dataTables.css' );	
		}
		
		add_action('wp_head', 'galio_compare_css');
	?>
    <?php wp_head() ?>

    <style type="text/css">
		table.compare-list tr.image th,
		table.compare-list tr.title th, 
		table.compare-list tr.price th, 
		table.compare-list tr.add-to-cart th {
			font-size: 15px !important;
			text-indent: 0 !important;
		}
        body.loading {
            background: url("<?php echo YITH_WOOCOMPARE_URL ?>assets/images/colorbox/loading.gif") no-repeat scroll center center transparent;
        }
		table.compare-list tr.price {
			width: 100%;
			font-size: 20px;
			color: #444;
			font-weight: bold;
			margin: 10px 0 12px;
		}
		table.compare-list tr.price > td {
			text-decoration: blink;
		}
		table.compare-list tr.price .old-price {
		  margin: 0;
		  color: #888888;
		  text-decoration: line-through;
		  font-size: 14px;
		}
		table.compare-list tr.price .old-price .amount {
		  margin: 0 5px 0 0;
		}
		table.compare-list tr.price .special-price {
		  display: inline-block;
		  margin: 0;
		}
		.price  {
			display: none;
		}	
		.price.repeated   {
			display: table-row;
		}
		.atc-notice-wrapper,.quickview-wrapper {
			display: none !important;
		}
		.vgwc-product-price,
		.product-price,
		.compare-list .price,
		.price {
		  font-weight: bold;
		  margin: 0px 0 5px;
		  font-size: 16px;
		}
		.vgwc-product-price.price2 .old-price,
		.vgwc-product-price.price2 del,
		.product-price.price2 .old-price,
		.product-price.price2 del,
		.compare-list .price.price2 .old-price,
		.compare-list .price.price2 del,
		.price.price2 .old-price,
		.price.price2 del {
		  margin-left: 9px;
		}
		.vgwc-product-price > del,
		.product-price > del,
		.compare-list .price > del,
		.price > del {
		  margin: 0;
		  color: #888;
		  text-decoration: line-through;
		  font-size: 14px;
		  line-height: 15px;
		  font-weight: normal;
		  opacity: 0.5 !important;
		  filter: alpha(opacity=50) !important;
		  margin: 0 0 0 5px;
		}
		.vgwc-product-price > .amount:first-child,
		.product-price > .amount:first-child,
		.compare-list .price > .amount:first-child,
		.price > .amount:first-child {
		  margin: 0 5px 0 0;
		}
		.vgwc-product-price > .amount:last-child,
		.product-price > .amount:last-child,
		.compare-list .price > .amount:last-child,
		.price > .amount:last-child {
		  margin: 0 0 0 5px;
		}
		.vgwc-product-price ins,
		.product-price ins,
		.compare-list .price ins,
		.price ins {
		  display: inline-block;
		  margin: 0;
		  text-decoration: blink !important;
		}
		.vgwc-product-price ins,
		.product-price ins,
		.compare-list .price ins,
		.price ins {
		  float: none;
		}
		.vgwc-product-price.price-variable .amount:first-child,
		.product-price.price-variable .amount:first-child,
		.compare-list .price.price-variable .amount:first-child,
		.price.price-variable .amount:first-child {
		  margin: 0 5px 0 0;
		}
		.vgwc-product-price.price-variable .amount:last-child,
		.product-price.price-variable .amount:last-child,
		.compare-list .price.price-variable .amount:last-child,
		.price.price-variable .amount:last-child {
		  margin: 0 0 0 5px;
		}
		.vgwc-product-price.price-variable del {
			display: inherit;
			margin-bottom: 6px;
		}
    </style>
</head>
<!-- END HEAD -->

<?php global $product; ?>

<!-- START BODY -->
<body <?php body_class('woocommerce') ?>>

<h1>
    <?php echo $localized_table_text ?>
    <?php if ( ! $is_iframe ) : ?><a class="close" href="#"><?php esc_html_e( 'Close window [X]', 'galio' ) ?></a><?php endif; ?>
</h1>

<?php do_action( 'yith_woocompare_before_main_table' ); ?>

<table class="compare-list" cellpadding="0" cellspacing="0"<?php if ( empty( $products ) ) echo ' style="width:100%"' ?>>
    <thead>
    <tr>
        <th>&nbsp;</th>
        <?php foreach( $products as $i => $product ) : ?>
            <td></td>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>&nbsp;</th>
        <?php foreach( $products as $i => $product ) : ?>
            <td></td>
        <?php endforeach; ?>
    </tr>
    </tfoot>
    <tbody>

    <?php if ( empty( $products ) ) : ?>

        <tr class="no-products">
            <td><?php esc_html_e( 'No products added in the compare table.', 'galio' ) ?></td>
        </tr>

    <?php else : ?>
        <tr class="remove">
            <th>&nbsp;</th>
            <?php foreach( $products as $i => $product ) : $product_class = ( $i % 2 == 0 ? 'odd' : 'even' ) . ' product_' . $product->id ?>
                <td class="<?php echo esc_attr($product_class); ?>">
                    <a href="<?php echo add_query_arg( 'redirect', 'view', $this->remove_product_url( $product->id ) ) ?>" data-product_id="<?php echo esc_attr($product->id); ?>"><?php esc_html_e( 'Remove', 'galio' ) ?> <span class="remove">x</span></a>
                </td>
            <?php endforeach ?>
        </tr>

        <?php foreach ( $fields as $field => $name ) : ?>

            <tr class="<?php echo esc_attr($field); ?>">

                <th>
                    <?php echo $name ?>
                    <?php if ( $field == 'image' ) echo '<div class="fixed-th"></div>'; ?>
                </th>

                <?php foreach( $products as $i => $product ) : $product_class = ( $i % 2 == 0 ? 'odd' : 'even' ) . ' product_' . $product->id; ?>
                    <td class="<?php echo esc_attr($product_class); ?>"><?php
                        switch( $field ) {

                            case 'image':
                                echo '<div class="image-wrap">' . wp_get_attachment_image( $product->fields[$field], 'yith-woocompare-image' ) . '</div>';
                                break;

                            case 'add-to-cart':
                                woocommerce_template_loop_add_to_cart();
                                break;

                            default:
                                echo empty( $product->fields[$field] ) ? '&nbsp;' : $product->fields[$field];
                                break;
                        }
                        ?>
                    </td>
                <?php endforeach ?>

            </tr>

        <?php endforeach; ?>

        <?php if ( $repeat_price == 'yes' && isset( $fields['price'] ) ) : ?>
            <tr class="price repeated">
                <th><?php echo $fields['price']; ?></th>

                <?php foreach( $products as $i => $product ) : $product_class = ( $i % 2 == 0 ? 'odd' : 'even' ) . ' product_' . $product->id ?>
                    <td class="<?php echo esc_attr($product_class); ?>"><?php echo $product->fields['price']; ?></td>
                <?php endforeach; ?>

            </tr>
        <?php endif; ?>

        <?php if ( $repeat_add_to_cart == 'yes' && isset( $fields['add-to-cart'] ) ) : ?>
            <tr class="add-to-cart repeated">
                <th><?php echo $fields['add-to-cart']; ?></th>

                <?php foreach( $products as $i => $product ) : $product_class = ( $i % 2 == 0 ? 'odd' : 'even' ) . ' product_' . $product->id ?>
                    <td class="<?php echo esc_attr($product_class); ?>"><?php woocommerce_template_loop_add_to_cart(); ?></td>
                <?php endforeach; ?>

            </tr>
        <?php endif; ?>

    <?php endif; ?>

    </tbody>
</table>

<?php do_action( 'yith_woocompare_after_main_table' ); ?>

<?php if( wp_script_is( 'responsive-theme', 'enqueued' ) ) wp_dequeue_script( 'responsive-theme' ) ?><?php if( wp_script_is( 'responsive-theme', 'enqueued' ) ) wp_dequeue_script( 'responsive-theme' ) ?>
<?php do_action('wp_print_footer_scripts'); ?>

<script type="text/javascript">

    jQuery(document).ready(function($){
        <?php if ( $is_iframe ) : ?>$('a').attr('target', '_parent');<?php endif; ?>

        var oTable;
        $('body').on( 'yith_woocompare_render_table', function(){
            if( $( window ).width() > 767 ) {
                oTable = $('table.compare-list').dataTable( {
                    "sScrollX": "100%",
                    //"sScrollXInner": "150%",
                    "bScrollInfinite": true,
                    "bScrollCollapse": true,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false,
                    "bFilter": false,
                    "bAutoWidth": false
                } );

                new FixedColumns( oTable );
                $('<table class="compare-list" />').insertAfter( $('h1') ).hide();
            }
        }).trigger('yith_woocompare_render_table');

        // add to cart
        var button_clicked;
        $(document).on('click', 'a.add_to_cart_button', function(){
            button_clicked = $(this);
            button_clicked.block({message: null, overlayCSS: {background: '#fff url(' + woocommerce_params.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});
        });

        // remove add to cart button after added
        $('body').on('added_to_cart', function( ev, fragments, cart_hash, button ){
            button_clicked.hide();

            <?php if ( $is_iframe ) : ?>
            $('a').attr('target', '_parent');

            // Replace fragments
            if ( fragments ) {
                $.each(fragments, function(key, value) {
                    $(key, window.parent.document).replaceWith(value);
                });
            }
            <?php endif; ?>
        });

        // close window
        $(document).on( 'click', 'a.close', function(e){
            e.preventDefault();
            window.close();
        });

        $(window).on( 'yith_woocompare_product_removed', function(){
            if( $( window ).width() > 767 ) {
                oTable.fnDestroy(true);
            }
            $('body').trigger('yith_woocompare_render_table');
        });

    });

</script>

</body>
</html>