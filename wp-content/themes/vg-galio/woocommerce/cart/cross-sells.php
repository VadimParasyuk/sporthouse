<?php
/**
 * Cross-sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

$galio_options  	  = galio_get_global_variables();
$product 			  = galio_get_global_variables('product');
$woocommerce 		  = galio_get_global_variables('woocommerce');
$woocommerce_loop  	  = galio_get_global_variables('woocommerce_loop');

$crosssells = WC()->cart->get_cross_sells();

if (sizeof($crosssells) == 0) return;

$crosssells_amount = isset($galio_options['crosssells_amount']) ? $galio_options['crosssells_amount'] : $posts_per_page;

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => apply_filters('woocommerce_cross_sells_total', $crosssells_amount),
	'orderby'             => $orderby,
	'post__in'            => $crosssells,
	'meta_query'          => $meta_query
);

$products = new WP_Query($args);

$woocommerce_loop['columns'] = apply_filters('woocommerce_cross_sells_columns', $columns);

$woocommerce_loop['columns'] = apply_filters('woocommerce_cross_sells_columns', $columns);

if ($products->have_posts()) : ?>

<div class="widget crosssells_products_widget <?php if(!empty($galio_options['crosssells_icon'])) echo 'vg-icon';?>">
	<div class="vg-title"><h3><?php
		if(!empty($galio_options['crosssells_icon'])) {
			echo '<i class="' .esc_attr($galio_options['crosssells_icon']) .'"></i>';
		}?><span><?php echo esc_html($galio_options['crosssells_title']); ?></span></h3></div>
		<div class="crosssells products">
			<?php woocommerce_product_loop_start(); ?>

				<?php while ($products->have_posts()) : $products->the_post(); ?>

					<?php wc_get_template_part('content', 'product'); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php endif;

wp_reset_postdata();