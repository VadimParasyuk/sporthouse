<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

// Find the category + category parent, if applicable 
$term = get_queried_object(); 
$parent_id = empty( $term->term_id ) ? 0 : $term->term_id; 

// NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( http://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
$args = array(
	'child_of'		=> $parent_id,
	'menu_order'	=> 'ASC',
	'hide_empty'	=> 0,
	'hierarchical'	=> 1,
	'taxonomy'		=> 'product_cat',
	'pad_counts'	=> 1
);
$product_subcategories = get_categories( $args  );

galio_get_header(); ?>
<?php
$galio_options  = galio_get_global_variables(); 
?>
<div class="main-container page-shop">
	<div class="page-content">
		<div class="container">
			<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action('woocommerce_before_main_content');
			?>
			<div class="row">
				<?php if($galio_options['sidebar_pos']=='left' || !isset($galio_options['sidebar_pos'])) :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
				
				<div id="archive-product" class="col-xs-12 col-md-<?php echo (is_active_sidebar( 'sidebar-category' )) ? 9 : 12 ; ?>">
					<div class="archive-border <?php if($galio_options['sidebar_pos']=='right') { echo ' border-right';} ?>">
						<?php
							/**
							* remove message from 'woocommerce_before_shop_loop' and show here
							*/
							do_action('woocommerce_show_message');
						?>	
						<div class="catheader-wrapper">
							<div class="cate-des">
								<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
									<header class="entry-header">
										<h1 class="entry-title"><?php woocommerce_page_title(); ?></h1>
									</header>

								<?php endif; ?>
													
								<?php do_action('woocommerce_archive_description'); ?>
							</div>
						</div>
						
						<?php if (have_posts()) : ?>
							
							
							<?php do_action('woocommerce_after_shop_header'); ?>
							
							<?php if((is_shop() && '' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && '' !== get_option('woocommerce_category_archive_display'))) : ?>
							<div class="all-subcategories">
								<?php woocommerce_product_subcategories(); ?>
								<div class="clearfix"></div>
							</div>							
							<?php endif; ?>
							
							<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) ||  is_product_tag()): ?>
							<div class="toolbar">
								<div class="view-mode">
									<a href="#" class="grid active" title="<?php echo esc_attr__('Grid', 'galio'); ?>"><i class="fa fa-th-large"></i> <strong><?php echo esc_html__('Grid', 'galio'); ?></strong></a>
									<a href="#" class="list" title="<?php echo esc_attr__('List', 'galio'); ?>"><i class="fa fa-th-list"></i> <strong><?php echo esc_html__('List', 'galio'); ?></strong></a>
								</div>
								<?php do_action('woocommerce_before_shop_loop'); ?>
								<div class="clearfix"></div>
							</div>
							<?php endif; ?>
							
							<?php woocommerce_product_loop_start(); ?>								
								<?php $woocommerce_loop['loop'] = 0; while (have_posts()) : the_post(); ?>
									<?php wc_get_template_part('content', 'product'); ?>
								<?php endwhile; // end of the loop. ?>
							<?php woocommerce_product_loop_end(); ?>
							
							<?php if((is_shop() && 'subcategories' !== get_option('woocommerce_shop_page_display')) || (is_product_category() && 'subcategories' !== get_option('woocommerce_category_archive_display')) || (empty($product_subcategories) && 'subcategories' == get_option('woocommerce_category_archive_display')) ||  is_product_tag()): ?>
							<div class="toolbar tb-bottom">
								<div class="view-mode">
									<a href="#" class="grid active" title="<?php echo esc_attr__('Grid', 'galio'); ?>"><i class="fa fa-th-large"></i> <strong><?php echo esc_html__('Grid', 'galio'); ?></strong></a>
									<a href="#" class="list" title="<?php echo esc_attr__('List', 'galio'); ?>"><i class="fa fa-th-list"></i> <strong><?php echo esc_html__('List', 'galio'); ?></strong></a>
								</div>
								<?php do_action('woocommerce_after_shop_loop'); ?>
								<div class="clearfix"></div>
							</div>
							<?php endif; ?>
							
						<?php elseif (! woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
							<?php wc_get_template('loop/no-products-found.php'); ?>
						<?php endif; ?>

					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action('woocommerce_after_main_content');
					?>

					<?php
						/**
						 * woocommerce_sidebar hook
						 *
						 * @hooked woocommerce_get_sidebar - 10
						 */
						//do_action('woocommerce_sidebar');
					?>
					</div>
				</div>
				
				<?php if($galio_options['sidebar_pos']=='right') :?>
					<?php get_sidebar('category'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php do_action('woocommerce_after_girdview'); ?>
<?php galio_get_footer(); ?>