<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

$woocommerce_loop  	=  galio_get_global_variables('woocommerce_loop');
$galio_options  	=  galio_get_global_variables();
$product  			=  galio_get_global_variables('product');

$upsells = $product->get_upsells();

if (sizeof($upsells) == 0) return;

$meta_query = WC()->query->get_meta_query();

$upsells_amount = isset($galio_options['upsells_amount']) ? $galio_options['upsells_amount'] : $posts_per_page;

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $upsells_amount,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array($product->id),
	'meta_query'          => $meta_query
);

$products = new WP_Query($args);

$woocommerce_loop['columns'] = 1;

if ($products->have_posts()) :
?>
<div class="widget upsells_products_widget <?php if(!empty($galio_options['upsells_icon'])) echo 'vg-icon';?>">
	<div class="vg-title"><h3><?php
		if(!empty($galio_options['upsells_icon'])) {
			echo '<i class="' .esc_attr($galio_options['upsells_icon']) .'"></i>';
		}?><span><?php echo esc_html($galio_options['upsells_title']); ?></span></h3></div>
	
	
	<div class="upsells products">

		<?php woocommerce_product_loop_start(); ?>

			<?php while ($products->have_posts()) : $products->the_post(); ?>

				<?php wc_get_template_part('content', 'product'); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>
</div>
<?php endif;

wp_reset_postdata();
