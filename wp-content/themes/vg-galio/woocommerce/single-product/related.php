<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if(! defined('ABSPATH')) exit; // Exit if accessed directly

$galio_options  		=  galio_get_global_variables();
$product  				=  galio_get_global_variables('product');
$woocommerce_loop  		=  galio_get_global_variables('woocommerce_loop');

$related = $product->get_related($posts_per_page);

if(sizeof($related) == 0) return;

$args = apply_filters('woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array($product->id)
));

$products = new WP_Query($args);

$woocommerce_loop['columns'] = 1;

if($products->have_posts()) :
?>

<div class="widget related_products_widget <?php if(!empty($galio_options['related_icon'])) echo 'vg-icon';?>">
	<div class="vg-title"><h3><?php
		if(!empty($galio_options['related_icon'])) {
			echo '<i class="' .esc_attr($galio_options['related_icon']) .'"></i>';
		}?><span><?php echo esc_html($galio_options['related_title']); ?></span></h3></div>
	
	<div class="related products">

		<?php woocommerce_product_loop_start(); ?>

			<?php while($products->have_posts()) : $products->the_post(); ?>

				<?php wc_get_template_part('content', 'product'); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>
</div>

<?php endif;

wp_reset_postdata();
