<?php
/**
 * @version    1.6
 * @package    VG Galio
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

$galio_options  = galio_get_global_variables(); 

galio_get_header();
?>
<?php 
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$bloglayout = 'blog-sidebar';

	$bloglayout = $_GET['layout'];
	switch($bloglayout) {
		case 'nosidebar':
			$blogclass = 'blog-nosidebar';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		case 'fullwidth':
			$blogclass = 'blog-fullwidth';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		default:
			$blogclass = 'blog-sidebar';
			$blogcolclass = 9;
	}
}
else {
	$blogsidebar = 'right';
	if(isset($galio_options['sidebarblog_pos']) && $galio_options['sidebarblog_pos']!=''){
		$blogsidebar = $galio_options['sidebarblog_pos'];
	}
	if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
		$blogsidebar = $_GET['sidebar'];
	}

	switch($blogsidebar) {
		case 'nosidebar':
			$blogclass = 'blog-nosidebar';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		case 'fullwidth':
			$blogclass = 'blog-fullwidth';
			$blogcolclass = 12;
			$blogsidebar = 'none';
			break;
		default:
			$blogclass = 'blog-sidebar';
			$blogcolclass = 9;
	}
}

?>
<div class="main-container page-wrapper">
	<div class="container">
		<?php galio_breadcrumb(); ?>
		<div class="row">
			<?php if($blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo  (is_active_sidebar('sidebar-1')) ? 'col-md-'. esc_attr($blogcolclass) : 'col-md-12'; ?>">
			
				<div class="page-content blog-page single <?php echo esc_attr($blogclass); if($blogsidebar=='left') {echo ' left-sidebar'; } if($blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php while (have_posts()) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('image-attachment'); ?>>
							<div class="entry-content">
								<div class="post-thumbnail">
									<div class="entry-attachment">
										<div class="attachment">
											<?php
											/*
											 * Grab the IDs of all the image attachments in a gallery so we can get the URL of the next adjacent image in a gallery,
											 * or the first image (if we're looking at the last image in a gallery), or, in a gallery of one, just the link to that image file
											 */
											$attachments = array_values(get_children(array('post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID')));
											foreach ($attachments as $k => $attachment) :
												if ($attachment->ID == $post->ID)
													break;
											endforeach;

											$k++;
											// If there is more than 1 attachment in a gallery
											if (count($attachments) > 1) :
												if (isset($attachments[ $k ])) :
													// get the URL of the next image attachment
													$next_attachment_url = get_attachment_link($attachments[ $k ]->ID);
												else :
													// or get the URL of the first image attachment
													$next_attachment_url = get_attachment_link($attachments[ 0 ]->ID);
												endif;
											else :
												// or, if there's only 1 image, get the URL of the image
												$next_attachment_url = wp_get_attachment_url();
											endif;
											?>
											<a href="<?php echo esc_url($next_attachment_url); ?>" title="<?php the_title_attribute(); ?>" rel="attachment"><?php
											/**
											 * Filter the image attachment size to use.
											 *
											 * @since VinaGecko 1.5
											 *
											 * @param array $size {
											 *     @type int The attachment height in pixels.
											 *     @type int The attachment width in pixels.
											 * }
											 */
											$attachment_size = apply_filters('galio_attachment_size', array(960, 960));
											echo wp_get_attachment_image($post->ID, $attachment_size);
											?></a>

											<?php if (! empty($post->post_excerpt)) : ?>
											<div class="entry-caption">
												<?php the_excerpt(); ?>
											</div>
											<?php endif; ?>
										</div><!-- .attachment -->

									</div><!-- .entry-attachment -->
								</div>
								<div class="entry-description">
									<?php the_content(); ?>
									<?php wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'galio'), 'after' => '</div>')); ?>
								</div><!-- .entry-description -->

							</div><!-- .entry-content -->
							<div class="postinfo-wrapper">
								<div class="post-date">
									<?php echo '<span class="day">'.get_the_date('d', $post->ID).'</span><span class="month"><span class="separator">/</span>'.get_the_date('M', $post->ID).'</span>' ;?>
								</div>
								<div class="post-info">
									<header class="entry-header">
										<h1 class="entry-title"><?php the_title(); ?></h1>
									</header><!-- .entry-header -->
									
									<footer class="entry-meta">
										<?php
											$metadata = wp_get_attachment_metadata();
											printf(esc_html__('<span class="meta-prep meta-prep-entry-date">Published </span> <span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span> at <a href="%3$s" title="Link to full-size image">%4$s &times; %5$s</a> in <a href="%6$s" title="Return to %7$s" rel="gallery">%8$s</a>.', 'galio'),
												esc_attr(get_the_date('c')),
												esc_html(get_the_date()),
												esc_url(wp_get_attachment_url()),
												$metadata['width'],
												$metadata['height'],
												esc_url(get_permalink($post->post_parent)),
												esc_attr(strip_tags(get_the_title($post->post_parent))),
												get_the_title($post->post_parent)
											);
										?>
										<?php edit_post_link(esc_html__('Edit', 'galio'), '<span class="edit-link">', '</span>'); ?>
									</footer><!-- .entry-meta -->
								</div>
							</div>
							
						</article><!-- #post -->

						<?php comments_template(); ?>
						
						<!--<nav id="image-navigation" class="navigation nav-single" role="navigation">
							<span class="previous-image nav-previous"><?php previous_image_link(false, esc_html_e('&larr; Previous', 'galio')); ?></span>
							<span class="next-image nav-next"><?php next_image_link(false, esc_html_e('Next &rarr;', 'galio')); ?></span>
						</nav> #image-navigation -->
						
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			
			<?php if($blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div>
</div>
<?php galio_get_footer(); ?>