<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('galio_theme_config')) {

    class galio_theme_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (true == Redux_Helpers::isTheme(__FILE__)) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html__('Section via hook', 'galio'),
                'desc' => wp_kses(__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'galio'), array('p' => array())),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
			);

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(esc_html__('Customize &#8220;%s&#8221;', 'galio'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'galio'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'galio'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html__('By %s', 'galio'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html__('Version %s', 'galio'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html__('Tags', 'galio') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                 printf(' <p class="howto">' . wp_kses(__('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'galio'), array('a' => array('href' => array(),'title' => array()))) . '</p>', esc_html__('http://codex.wordpress.org/Child_Themes', 'galio'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                Redux_Functions::initWpFilesystem();
                
                global $wp_filesystem;

                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }
            // General
            $this->sections[] = array(
                'title'     => esc_html__('General', 'galio'),
                'desc'      => esc_html__('General theme options', 'galio'),
                'icon'      => 'el-icon-cog',
                'fields'    => array(

                    array(
                        'id'        => 'logo_main',
                        'type'      => 'media',
                        'title'     => esc_html__('Logo', 'galio'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload logo here.', 'galio'),
					),
					array(
                        'id'        => 'logo_text',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Text', 'galio'),
                        'default'   => ''
					),
					array(
                        'id'        => 'logo_erorr',
                        'type'      => 'media',
                        'title'     => esc_html__('Logo for error 404 page', 'galio'),
                        'compiler'  => 'true',
                        'mode'      => false,
					),
					array(
                        'id'        => 'opt-favicon',
                        'type'      => 'media',
                        'title'     => esc_html__('Favicon', 'galio'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload favicon here.', 'galio'),
						'default'  => array( 'url' => get_template_directory_uri() . '/images/favicon.png' ),
					),
					array(
						'id'		=>'share_head_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis head tag', 'galio'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis head tag here', 'galio'),
						'default' 	=> '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553dd7dd1ff880d4" async="async"></script>',
					),
					array(
						'id'		=>'share_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis code', 'galio'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis code here', 'galio'),
						'default' 	=> '<div class="addthis_native_toolbox"></div>'
					),
					array(
                        'id'        => 'galio_show_author',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Author Info Page', 'galio'),
						'default'   => false,
					),		
					array(
                        'id'        => 'galio_loading',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Loading Page', 'galio'),
						'default'   => false,
					),
				),
			);
			// Background
            $this->sections[] = array(
                'title'     => esc_html__('Background', 'galio'),
                'desc'      => esc_html__('Use this section to upload background images, select background color', 'galio'),
                'icon'      => 'el-icon-picture',
                'fields'    => array(
					
					array(
                        'id'        => 'background_opt',
                        'type'      => 'background',
                        'output'    => array('body'),
                        'title'     => esc_html__('Body Background', 'galio'),
                        'subtitle'  => esc_html__('Body background with image, color. Only work with box layout', 'galio'),
						'default'   => '#ffffff',
					),
				),
			);
			// Colors
            $this->sections[] = array(
                'title'     => esc_html__('Presets', 'galio'),
                'desc'      => esc_html__('Presets options', 'galio'),
                'icon'      => 'el-icon-tint',
			);
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets1', 'galio'),
                'desc'     		=> esc_html__('Presets1 options', 'galio'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #d8373e).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#d8373e',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'text_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #404040).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404040',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Cart', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: #a5a5a5).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#a5a5a5',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'top_bottom_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Top Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position top bottom (default: #404048).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404048',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'botom_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #eaeaea).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#eaeaea',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #e1e1e1).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#e1e1e1',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'sale_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #d8373e).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#d8373e',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #f9af51).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#f9af51',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #ffba00).', 'galio'),
						'transparent' 	=> false,
                        'default'  		=> '#ffba00',
                        'validate'  	=> 'color',
					),
				),
			);	
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets2', 'galio'),
                'desc'     		=> esc_html__('Presets2 options', 'galio'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #189f2b).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#189f2b',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'text2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #404040).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404040',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Cart', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: #a5a5a5).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#a5a5a5',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'top_bottom2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Top Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position top bottom (default: #404048).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404048',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'botom2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #eaeaea).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#eaeaea',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #e1e1e1).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#e1e1e1',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'sale2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #189f2b).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#189f2b',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured2_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #DA4147).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#DA4147',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate2_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #ffba00).', 'galio'),
						'transparent' 	=> false,
                        'default'  		=> '#ffba00',
                        'validate'  	=> 'color',
					),
				),
			);		
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets3', 'galio'),
                'desc'     		=> esc_html__('Presets3 options', 'galio'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #6bc59a).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#6bc59a',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'text3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #404040).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404040',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Cart', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: #a5a5a5).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#a5a5a5',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'top_bottom3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Top Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position top bottom (default: #404048).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404048',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'botom3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #eaeaea).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#eaeaea',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #e1e1e1).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#e1e1e1',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'sale3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #6bc59a).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#6bc59a',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured3_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #CCC3BD).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#CCC3BD',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate3_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #ffba00).', 'galio'),
						'transparent' 	=> false,
                        'default'  		=> '#ffba00',
                        'validate'  	=> 'color',
					),
				),
			);		
			$this->sections[] = array(
                'title'     	=> esc_html__('Presets4', 'galio'),
                'desc'     		=> esc_html__('Presets4 options', 'galio'),
                'icon'      	=> 'el-icon-tint',
				'subsection' 	=> true,
                'fields'    	=> array(
					array(
                        'id'        	=> 'primary4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #459fdd).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#459fdd',
                        'validate'  	=> 'color',
					),	
					array(
                        'id'        	=> 'text4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Primary Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for primary color (default: #404040).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404040',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'cart4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Cart', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background cart (default: #a5a5a5).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#a5a5a5',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'top_bottom4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Top Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position top bottom (default: #404048).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404048',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'botom4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Bottom', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position bottom (default: #eaeaea).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#eaeaea',
                        'validate'  	=> 'color',
					),			
					array(
                        'id'        	=> 'footer4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Background Position Footer', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for background position Footer (default: #e1e1e1).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#e1e1e1',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'sale4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Sale', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Sale (default: #459fdd).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#459fdd',
                        'validate'  	=> 'color',
					),		
					array(
                        'id'        	=> 'featured4_color',
                        'type'      	=> 'color',
                        'title'     	=> esc_html__('Color Label Hot', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for color Label Hot (default: #404040).', 'galio'),
						'transparent' 	=> false,
                        'default'   	=> '#404040',
                        'validate'  	=> 'color',
					),
					array(
                        'id'        	=> 'rate4_color',
                        'type'      	=> 'color',
                        //'output'    	=> array(),
                        'title'     	=> esc_html__('Rating Star Color', 'galio'),
                        'subtitle'  	=> esc_html__('Pick a color for star of rating (default: #ffba00).', 'galio'),
						'transparent' 	=> false,
                        'default'  		=> '#ffba00',
                        'validate'  	=> 'color',
					),
				),
			);
			
			//Header
			$this->sections[] = array(
                'title'     => esc_html__('Header', 'galio'),
                'desc'      => esc_html__('Header options', 'galio'),
                'icon'      => 'el-icon-tasks',
			);
			
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Mini Cart', 'galio' ),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'mini_cart_sub_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Mini cart Sub title', 'galio'),
                        'default'   => 'Total'
					),		
				),
			);
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Top Shipping', 'galio' ),
				'subsection' => true,
				'fields'     => array(
					array(
						'id'		=>'vg_shipping',
						'type' 		=> 'editor',
						'title' 	=> esc_html__('Top Header Shipping', 'galio'),
						'default' 	=> '<div class="header-block-static"><div class="row"><div class="box-col box-col-1 col-md-4 col-sm-4 col-sms-12"><div class="box-col-inner"><div class="pull-left fa fa-clock-o"></div><div class="media-body"><h3>Working time</h3>Mon- Sun: 8.00 - 18.00</div></div></div><div class="box-col box-col-2 col-md-4 col-sm-4 col-sms-12"><div class="box-col-inner"><div class="pull-left fa fa fa-truck"></div><div class="media-body"><h3>Free shipping</h3>On order over $199</div></div></div><div class="box-col box-col-3 col-md-4 col-sm-4 col-sms-12"><div class="box-col-inner"><div class="pull-left fa fa-money"></div><div class="media-body"><h3>Money back 100%</h3>Within 30 Days after delivery</div></div></div></div></div>',
					),	
					array(
                        'id'        => 'vg_shipping_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Top Header Shipping', 'galio'),
						'default'   => true,
					),
				),
			);		
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Menu Mobile', 'galio' ),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'title_mobile_menu',
                        'type'      => 'text',
                        'title'     => esc_html__('Title Mobile Menu', 'galio'),
                        'default'   => 'Menu'
					),
				),
			);	

			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Max Menu Item', 'galio' ),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'show_menu_max',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Max Category Product Item', 'galio'),
						'default'   => true,
					),
					array(
                        'id'        => 'title_more_categories',
                        'type'      => 'text',
                        'title'     => esc_html__('Title More Categories', 'galio'),
                        'default'   => 'More Categories'
					),
					array(
                        'id'        => 'title_close_menu',
                        'type'      => 'text',
                        'title'     => esc_html__('Title Close Menu', 'galio'),
                        'default'   => 'Close Menu'
					),	
					array(
                        'id'        => 'menu_max_number',
                        'type'      => 'text',
                        'title'     => esc_html__('Max number item', 'galio'),
                        'default'   => '8'
					),
				),
			);
			//Social 
			$this->sections[] = array(
				'title'     => esc_html__('Top Bottom', 'galio'),
				'des'       => esc_html__('Top Bottom options', 'galio'),
				'icon'      => 'el-icon-cog',
			);
			
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Newsletter', 'galio' ),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'newsletter_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Newsletter', 'galio'),
						'default'   => true,
					),
					array(
                        'id'        => 'newsletter_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Newsletter title', 'galio'),
                        'default'   => 'SIGN UP FOR NEWSLLETTER'
                    ),	
					array(
                        'id'        => 'newsletter_des',
                        'type'      => 'text',
                        'title'     => esc_html__('Newsletter Description', 'galio'),
                        'default'   => 'Sign Up for Our Newsletter'
                    ),
					array(
						'id'       => 'newsletter_form',
						'type'     => 'text',
						'title'    => esc_html__('Newsletter form ID', 'galio'),
						'subtitle' => esc_html__('The form ID of MailPoet plugin.', 'galio'),
						'validate' => 'numeric',
						'msg'      => 'Please enter a form ID',
						'default'  => '1'
					),
				)
			);
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__('Social Icons', 'galio'),
				'subsection' => true,
				'fields'     => array(
					array(
                        'id'        => 'social_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Social', 'galio'),
						'default'   => true,
					),
					array(
						'id'       => 'ftsocial_icons',
						'type'     => 'sortable',
						'title'    => esc_html__('Footer social Icons', 'galio'),
						'subtitle' => esc_html__('Enter social links', 'galio'),
						'desc'     => esc_html__('Drag/drop to re-arrange', 'galio'),
						'mode'     => 'text',
						'options'  => array(
							'facebook'    => 'https://www.facebook.com/vinawebsolutions',
							'twitter'     => 'https://twitter.com/vnwebsolutions',
							'instagram'   => 'Instagram',
							'tumblr'      => 'Tumblr',
							'pinterest'   => 'Pinterest',
							'google-plus' => 'https://plus.google.com/',
							'linkedin'    => 'Linkedin',
							'behance'     => 'Behance',
							'dribbble'    => 'Dribbble',
							'youtube'     => 'https://youtube.com/',
							'vimeo'       => 'Vimeo',
							'rss'         => 'RSS',
						),
						'default' => array(
						    'facebook'    => 'https://www.facebook.com/vinawebsolutions',
							'twitter'     => 'https://twitter.com/vnwebsolutions',
							'instagram'   => '',
							'tumblr'      => '',
							'pinterest'   => '',
							'google-plus' => 'https://plus.google.com/+HieuJa/posts',
							'linkedin'    => '',
							'behance'     => '',
							'dribbble'    => '',
							'youtube'     => 'https://www.youtube.com/user/vinawebsolutions',
							'vimeo'       => '',
							'rss'         => '',
						),
					),
				)
			);
			
			//Footer
			$this->sections[] = array(
                'title'     => esc_html__('Footer', 'galio'),
                'desc'      => esc_html__('Footer options', 'galio'),
                'icon'      => 'el-icon-cog',
                'fields'    => array(
					array(
                        'id'        => 'copyright_show',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Copyright', 'galio'),
						'default'   => true,
					),
					array(
                        'id'        => 'copyright-notice',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Copyright Notice', 'galio'),
                        'default'   => 'Copyright (C) 2016 {VinaGecko.com}. All Rights Reserved.'
					),
					array(
                        'id'        => 'copyright-link',
                        'type'      => 'text',
                        'title'     => esc_html__('Copyright Link', 'galio'),
                        'default'   => 'http://vinagecko.com'
					),
					array(
                        'id'        => 'footer_payment',
                        'type'      => 'media',
                        'title'     => esc_html__('Image Payment', 'galio'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload Image Payment here.', 'galio'),
						'default'  => array( 'url' => get_template_directory_uri() . '/images/payment.png' ),
					),
				),
			);
			
			
			//Fonts
			$this->sections[] = array(
                'title'     => esc_html__('Fonts', 'galio'),
                'desc'      => esc_html__('Fonts options', 'galio'),
                'icon'      => 'el-icon-font',
                'fields'    => array(

                    array(
                        'id'            	=> 'bodyfont',
                        'type'          	=> 'typography',
                        'title'         	=> esc_html__('Body font', 'galio'),
                        //'compiler'      	=> true,  // Use if you want to hook in your own CSS compiler
                        'google'        	=> true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   	=> false,    // Select a backup non-google font in addition to a google font
                        'font-style'    	=> true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       	=> true, // Only appears if google is true and subsets not set to false
                        //'font-size'     	=> false,
                        //'line-height'   	=> false,
                        //'word-spacing'  	=> true,  // Defaults to false
                        //'letter-spacing'	=> true,  // Defaults to false
                        'color'         	=> false,
                        //'preview'       	=> false, // Disable the previewer
                        'all_styles'    	=> true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        	=> array('body'), // An array of CSS selectors to apply this font style to dynamically
                        //'compiler'      	=> array('h2.site-description-compiler'), // An array of CSS selectors to apply this font style to dynamically
                        'units'         	=> 'px', // Defaults to px
                        'subtitle'      	=> esc_html__('Main body font.', 'galio'),
                        'default'       	=> array(
                            'font-weight'   => '400',
                            'font-family'   => 'Open Sans, sans-serif',
                            'google'        => true,
                            'font-size'     => '13px',
                            'line-height'   => '20px'),
					),
				),
			);
			
			// Layout
            $this->sections[] = array(
                'title'     => esc_html__('Layout', 'galio'),
                'icon'      => 'el-icon-align-justify',
                'fields'    => array(
					array(
						'id'       => 'page_layout',
						'subtitle' => esc_html__('Select page default page layout.', 'galio'),
						'type'     => 'select',
						'multi'    => false,
						'title'    => esc_html__('Page Layout', 'galio'),
						'options'  => array(
							'layout-1' => 'Page Layout 01',
							'layout-2' => 'Page Layout 02',
							'layout-3' => 'Page Layout 03',
							'layout-4' => 'Page Layout 04',
							'layout-5' => 'Page Layout 05',
							'layout-6' => 'Page Layout 06',
						),
						'default'  => 'layout-1'
					),		
					array(
						'id'       => 'page_style',
						'subtitle' => esc_html__('Select layout style: Box or Full Width', 'galio'),
						'type'     => 'select',
						'multi'    => false,
						'title'    => esc_html__('Layout Style', 'galio'),
						'options'  => array(
							'full' => 'Full Width',
							'box'  => 'Box'
						),
						'default'  => 'full'
					),	
					array(
                        'id'        => 'preset_option',
                        'type'      => 'select',
                        'title'     => esc_html__('Preset', 'galio'),
						'subtitle'      => esc_html__('Select a preset to quickly apply pre-defined colors and fonts', 'galio'),
                        'options'   => array(
							'1' => 'Preset 1',
                            '2' => 'Preset 2',
							'3' => 'Preset 3',
							'4' => 'Preset 4',
                      ),
                        'default'   => '1'
					),
					array(
                        'id'        => 'enable_sswitcher',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Style Switcher', 'galio'),
						'subtitle'  => esc_html__('The style switcher is only for preview on front-end', 'galio'),
						'default'   => false,
					),
				),
			);
			
			//Brand logos
			$this->sections[] = array(
                'title'     => esc_html__('Brand Logos', 'galio'),
                'desc'      => esc_html__('Upload brand logos and links', 'galio'),
                'icon'      => 'el-icon-cog',
                'fields'    => array(
					array(
                        'id'        => 'enable_brands',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Brand Logos', 'galio'),
						'subtitle'  => esc_html__('The Brand Logos is only for preview on front-end', 'galio'),
						'default'   => true,
					),
					array(
                        'id'        => 'brands_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Brands title', 'galio'),
                        'default'   => 'Popular Brand',
                    ),	
					array(
                        'id'        => 'brands_icon',
                        'type'      => 'text',
                        'title'     => esc_html__('Brands icon', 'galio'),
                        'default'   => 'fa fa-crop',
                    ),	
					array(
						'id'          => 'brand_logos',
						'type'        => 'slides',
						'title'       => esc_html__('Logos', 'galio'),
						'desc'        => esc_html__('Upload logo image and enter logo link.', 'galio'),
						'placeholder' => array(
							'title'           => esc_html__('Title', 'galio'),
							'description'     => esc_html__('Description', 'galio'),
							'url'             => esc_html__('Link', 'galio'),
						),
					),
				),
			);
			
			// Sidebar
			$this->sections[] = array(
                'title'     => esc_html__('Sidebar', 'galio'),
                'desc'      => esc_html__('Sidebar options', 'galio'),
                'icon'      => 'el-icon-cog',
                'fields'    => array(
					array(
						'id'       	=> 'sidebar_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Main Sidebar Position', 'galio'),
						'subtitle'      => esc_html__('Sidebar on category page', 'galio'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'left'
					),
					array(
						'id'       	=> 'sidebar_product',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Product Sidebar Position', 'galio'),
						'subtitle'      => esc_html__('Sidebar on product page', 'galio'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'right'
					),
					array(
						'id'       	=> 'sidebarse_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Secondary Sidebar Position', 'galio'),
						'subtitle'  => esc_html__('Sidebar on pages', 'galio'),
						'options'  	=> array(
							'left' 	=> 'Left',
							'right' => 'Right'),
						'default'  	=> 'left'
					),
					array(
						'id'       	=> 'sidebarblog_pos',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Blog Sidebar Position', 'galio'),
						'subtitle'  => esc_html__('Sidebar on Blog pages', 'galio'),
						'options'  			=> array(
							'left' 			=> 'Left',
							'right'		    => 'Right',
							'nosidebar' 	=> 'Nosidebar',
							'fullwidth' 	=> 'Fullwidth',
						),
						'default'  	=> 'right'
					),
				),
			);
		  
			// Product
			$this->sections[] = array(
                'title'     => esc_html__('Product', 'galio'),
                'desc'      => esc_html__('Use this section to select options for product', 'galio'),
                'icon'      => 'el-icon-tags',
				'fields'    => array(
					array(
						'id'       => 'second_image',
						'type'     => 'switch',
						'title'    => esc_html__('Use secondary product image', 'galio'),
						'default'  => true,
					),		
					array(
						'id'       => 'quick_view',
						'type'     => 'switch',
						'title'    => esc_html__('Use quick view product', 'galio'),
						'default'  => true,
					),	
					array(
						'id'       	=> 'sale_label',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Sale Label Settings', 'galio'),
						'subtitle'      => esc_html__('Config Sale Label Settings', 'galio'),
						'options'  	=> array(
							'Sale' 						=> esc_html__('Sale', 'galio'),
							'Save {percent-diff}%' 	=> esc_html__('Save {percent-diff}%', 'galio'),
							'Save ${price-diff}' 	=> esc_html__('Save ${price-diff}', 'galio'),
							'custom' 				=> esc_html__('Custom (use field below)', 'galio'),
						),
						'default'  	=> 'custom'
					),
					array(
                        'id'        => 'sale_label_custom',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Format Label', 'galio'),
                        'desc'     => esc_html__('{price-diff} inserts the dollar amount off, {percent-diff} inserts the percent reduction (rounded).', 'galio'),
                        'default'   => '{percent-diff}%'
					),		
					array(
                        'id'        => 'featured_label_custom',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Featured Label', 'galio'),
                        'desc'     => esc_html__('Text Custom Featured Label ', 'galio'),
                        'default'   => 'Hot'
					),
				),
			);
		  
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Category', 'galio' ),
				'desc'      => esc_html__('Use this section to select options for Page Category Product', 'galio'),
				'subsection' => true,	
                'fields'    => array(
					array(
						'id'       	=> 'layout_product',
						'type'     	=> 'radio',
						'title'    	=> esc_html__('Category View Layout', 'galio'),
						'subtitle'      => esc_html__('View layout on category page', 'galio'),
						'options'  	=> array(
							'gridview' 	=> 'Grid View',
							'listview' => 'List View'),
						'default'  	=> 'gridview'
					),
					array(
                        'id'        => 'cat_banner_img',
                        'type'      => 'media',
                        'title'     => esc_html__('Banner Header Category', 'galio'),
                        'compiler'  => 'true',
                        'mode'      => false,
                        'desc'      => esc_html__('Upload banner category here.', 'galio'),
						'default'  => array( 'url' => get_template_directory_uri() . '/images/banner_static1.jpg' ),
					),
					array(
                        'id'        => 'cat_banner_link',
                        'type'      => 'text',
                        'title'     => esc_html__('Link Banner Category', 'galio'),
                        'default'   => ''
					),	
					array(
                        'id'        => 'cat_description',
                        'type'      => 'editor',
                        'title'     => esc_html__('Description Category', 'galio'),
                        'default'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor gravida dictum. In egestas convallis nisi, quis mattis ligula fermentum auctor. Vivamus vitae neque suscipit, ornare tellus id, mattis ante. Suspendisse vitae pretium quam. Ut suscipit nec turpis ut pulvinar. Sed sed ornare arcu. In non tempus erat. Nunc ullamcorper mauris id metus pellentesque, in rutrum nisl posuere. Integer in nibh vel massa semper pulvinar.'
					),
					array(
						'id'        	=> 'product_per_page',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Products per page', 'galio'),
						'subtitle'  	=> esc_html__('Amount of products per page on category page', 'galio'),
						"default"   	=> 16,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 48,
						'display_value' => 'text'
					),
					array(
						'id'        	=> 'product_per_row',
						'handles'		=>	true,
						'type'      	=> 'select',
						'title'     	=> esc_html__('Products per row', 'galio'),
						'subtitle'  	=> esc_html__('Products per row on category page', 'galio'),
                        'options'   => array(
							'3' => '3',
                            '4' => '4',
						),
                        'default'   => '4'
					),
				),
			);
			
			$this->sections[] = array(
				'icon'       => 'el-icon-website',
				'title'      => esc_html__( 'Single Product', 'galio' ),
				'desc'      => esc_html__('Use this section to select options for Page Category Product', 'galio'),
				'subsection' => true,	
                'fields'    => array(
					array(
						'id'       => 'single_meta',
						'type'     => 'switch',
						'title'    => esc_html__('Use hidden meta tags, category, SKU', 'galio'),
						'default'  => true,
					),
					array(
                        'id'        => 'crosssells_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Crosssells products title', 'galio'),
                        'default'   => 'Crosssells Products'
					),	
					array(
						'id'       => 'crosssells_icon',
						'type'     => 'select',
						'data'     => 'elusive-icons',
						'title'    => esc_html__( 'Crosssells Icons Select Option', 'galio' ),
						'default'  => 'el el-asl'
					),
					array(
						'id'        	=> 'crosssells_amount',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Number of crosssells products', 'galio'),
						"default"   	=> 6,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 16,
						'display_value' => 'text'
					),		
					array(
                        'id'        => 'upsells_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Upsells products title', 'galio'),
                        'default'   => 'Upsells Products'
					),	
					array(
						'id'       => 'upsells_icon',
						'type'     => 'select',
						'data'     => 'elusive-icons',
						'title'    => esc_html__( 'Upsells Icons Select Option', 'galio' ),
						'default'  => 'el el-bookmark'
					),
					array(
						'id'        	=> 'upsells_amount',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Number of upsells products', 'galio'),
						"default"   	=> 6,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 16,
						'display_value' => 'text'
					),
					array(
                        'id'        => 'related_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Related products title', 'galio'),
                        'default'   => 'Related Products'
					),
					array(
						'id'       => 'related_icon',
						'type'     => 'select',
						'data'     => 'elusive-icons',
						'title'    => esc_html__( 'Related Icons Select Option', 'galio' ),
						'default'  => 'el el-screen'
					),
					array(
						'id'        	=> 'related_amount',
						'type'      	=> 'slider',
						'title'     	=> esc_html__('Number of related products', 'galio'),
						"default"   	=> 6,
						"min"       	=> 3,
						"step"      	=> 1,
						"max"       	=> 16,
						'display_value' => 'text'
					),
					
					array(
						'id'		=>'share_head_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis head tag', 'galio'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis head tag here', 'galio'),
						'default' 	=> '',
					),
					array(
						'id'		=>'share_code',
						'type' 		=> 'textarea',
						'title' 	=> esc_html__('ShareThis/AddThis code', 'galio'), 
						'desc' 		=> esc_html__('Paste your ShareThis or AddThis code here', 'galio'),
						'default' 	=> ''
					),
				),
			);
			
			// Less Compiler
            $this->sections[] = array(
                'title'     => esc_html__('Less Compiler', 'galio'),
                'desc'      => esc_html__('Turn on this option to apply all theme options. Turn of when you have finished changing theme options and your site is ready.', 'galio'),
                'icon'      => 'el-icon-wrench',
                'fields'    => array(
					array(
                        'id'        => 'enable_less',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Less Compiler', 'galio'),
						'default'   => true,
					),
				),
			);
			
            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . wp_kses(__('<strong>Theme URL:</strong> ', 'galio'), array('strong' => array())) . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . wp_kses(__('<strong>Author:</strong> ', 'galio'), array('strong' => array())) . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . wp_kses(__('<strong>Version:</strong> ', 'galio'), array('strong' => array())) . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs 		 = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . wp_kses(__('<strong>Tags:</strong> ', 'galio'), array('strong' => array())) . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            $this->sections[] = array(
                'icon'              => 'el-icon-list-alt',
                'title'             => esc_html__('Customizer Only', 'galio'),
                'desc'              => wp_kses(__('<p class="description">This Section should be visible only in Customizer</p>', 'galio'), array('p' => array('class' => array()))),
                'customizer_only'   => true,
                'fields'    => array(
                    array(
                        'id'        => 'opt-customizer-only',
                        'type'      => 'select',
                        'title'     => esc_html__('Customizer Only Option', 'galio'),
                        'subtitle'  => esc_html__('The subtitle is NOT visible in customizer', 'galio'),
                        'desc'      => esc_html__('The field desc is NOT visible in customizer.', 'galio'),
                        'customizer_only'   => true,

                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Opt 1',
                            '2' => 'Opt 2',
                            '3' => 'Opt 3'
						),
                        'default'   => '2'
					),
				)
			);            
            
            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'galio'),
                'desc'      => esc_html__('Import and Export your Redux Framework settings from file, text or URL.', 'galio'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
					),
				),
			);

            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_html__('Theme Information', 'galio'),
                //'desc'      => esc_html__('<p class="description">This is the Description. Again HTML is allowed</p>', 'galio'),
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
					)
				),
			);
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html__('Theme Information 1', 'galio'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'galio'), array('p' => array()))
			);

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html__('Theme Information 2', 'galio'),
                'content'   => wp_kses(__('<p>This is the tab content, HTML is allowed.</p>', 'galio'), array('p' => array()))
			);

            // Set the help sidebar
            $this->args['help_sidebar'] = wp_kses(__('<p>This is the sidebar content, HTML is allowed.</p>', 'galio'), array('p' => array()));
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'galio_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'galio'),
                'page_title'        => esc_html__('Theme Options', 'galio'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' 	=> '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => true,                    // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'          => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'       => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
					),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
					),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
						),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
						),
					),
				)
			);


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
			);
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
			);
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
			);
            $this->args['share_icons'][] = array(
                'url'   => 'http://www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
			);

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
                //$this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'galio'), $v);
            } else {
                //$this->args['intro_text'] = esc_html__('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'galio');
            }

            // Add content after the form.
            //$this->args['footer_text'] = esc_html__('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'galio');
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new galio_theme_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
